load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive", "http_file")
load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")

def repositories():
    maybe(
        http_archive,
        name = "io_bazel_rules_docker",
        strip_prefix = "rules_docker-0.14.4",
        urls = ["https://github.com/bazelbuild/rules_docker/releases/download/v0.14.4/rules_docker-v0.14.4.tar.gz"],
        sha256 = "4521794f0fba2e20f3bf15846ab5e01d5332e587e9ce81629c7f96c793bb7036",
    )
    maybe(
        http_archive,
        name = "build_bazel_rules_nodejs",
        url = "https://github.com/bazelbuild/rules_nodejs/releases/download/3.8.0/rules_nodejs-3.8.0.tar.gz",
        sha256 = "e79c08a488cc5ac40981987d862c7320cee8741122a2649e9b08e850b6f20442",
    )
    maybe(
        http_archive,
        name = "rules_pkg",
        urls = [
            "https://github.com/bazelbuild/rules_pkg/releases/download/0.2.6-1/rules_pkg-0.2.6.tar.gz",
        ],
        sha256 = "aeca78988341a2ee1ba097641056d168320ecc51372ef7ff8e64b139516a4937",
    )
    maybe(
        http_file,
        name = "jq_linux",
        executable = 1,
        urls = ["https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64"],
        sha256 = "af986793a515d500ab2d35f8d2aecd656e764504b789b66d7e1a0b727a124c44",
    )
    maybe(
        http_file,
        name = "jq_osx",
        executable = 1,
        urls = ["https://github.com/stedolan/jq/releases/download/jq-1.6/jq-osx-amd64"],
        sha256 = "5c0a0a3ea600f302ee458b30317425dd9632d1ad8882259fcaf4e9b868b2b1ef",
    )
    maybe(
        http_archive,
        name = "kustomize_linux",
        urls = ["https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv3.8.1/kustomize_v3.8.1_linux_amd64.tar.gz"],
        sha256 = "9d5b68f881ba89146678a0399469db24670cba4813e0299b47cb39a240006f37",
        build_file_content = "exports_files([\"kustomize\"])",
    )
    maybe(
        http_archive,
        name = "kustomize_osx",
        urls = ["https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv3.8.1/kustomize_v3.8.1_darwin_amd64.tar.gz"],
        sha256 = "db8a283c7edfc10c0e661b31916f4e60096eae558657730177e4f103c84ddc00",
        build_file_content = "exports_files([\"kustomize\"])",
    )
    maybe(
        http_file,
        name = "yq_linux",
        executable = 1,
        urls = ["https://github.com/mikefarah/yq/releases/download/v4.4.1/yq_linux_amd64"],
        sha256 = "42826789ba1f42ef9effd154090f48e7a944554163e83b5ac2e06ad5e775fabf",
    )
    maybe(
        http_file,
        name = "yq_osx",
        executable = 1,
        urls = ["https://github.com/mikefarah/yq/releases/download/v4.4.1/yq_darwin_amd64"],
        sha256 = "48e156bc078b6664d1e59bab295241d05a0b4a48fbf54cc2788e3ac58ba99e0d",
    )
