load("@io_bazel_rules_docker//repositories:repositories.bzl", docker_repositories = "repositories")

def rules_docker_setup():
    docker_repositories()

def setup():
    rules_docker_setup()
