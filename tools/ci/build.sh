#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

# Tag npm package
echo "Tagging apps/farmer package ..."
bazel build //services/apps/farmer:version
cp $(bazel info bazel-genfiles)/services/apps/farmer/versioned-package.json $(dirname $0)/../../services/apps/farmer/package.json
echo OK

# Build the app with Qasar
echo "Building apps/farmer with Quasar ..."
bazel run --run_under "cd $(pwd)/services/apps/farmer && " @apps_farmer_npm//@quasar/app/bin:quasar -- build
echo OK

bazel build //...
