load("@io_bazel_rules_docker//container:container.bzl", "container_image")
load("@rules_pkg//:pkg.bzl", "pkg_tar")

package(default_visibility = ["//services/apps:__pkg__"])

container_image(
    name = "image",
    base = "@nginx_image_base//image",
    env = {
        "PORT": "80",
    },
    tars = [
        ":conf",
        ":nginx",
        ":spa_sources",
    ],
)

pkg_tar(
    name = "conf",
    srcs = glob([
        "public/conf/*.json",
    ]),
    package_dir = "/usr/share/nginx/html/conf",
    strip_prefix = "public/conf",
)

pkg_tar(
    name = "nginx",
    srcs = [
        "nginx.conf",
    ],
    package_dir = "/etc/nginx",
)

pkg_tar(
    name = "spa_sources",
    srcs = glob([
        "dist/spa/**/*",
    ]),
    package_dir = "/usr/share/nginx/html",
    strip_prefix = "dist/spa",
)

genrule(
    name = "version",
    srcs = [
        "//:stable-status-farmer-mobile-app-version.txt",
        "package.json",
    ],
    outs = [
        "versioned-package.json",
    ],
    cmd = ";".join([
        "sed \"s/\\\"version\\\": \\\".*\\\"/\\\"version\\\": \\\"$$(cat $(location //:stable-status-farmer-mobile-app-version.txt))\\\"/g\" $(location package.json) > $@",
    ]),
    stamp = True,
)

filegroup(
    name = "kustomize_resources",
    srcs = [
        "kustomization.yaml",
    ] + glob([
        "deploy/base/**/*",
    ]),
)

# RULE AUTOMATICALLY GENERATED DO NOT EDIT!
filegroup(
    name = "graphql_queries",
    srcs = glob(["**/*.gql","**/*.graphql"],exclude=["**/tests/**/*.gql","**/tests/**/*.graphql"]),
)
