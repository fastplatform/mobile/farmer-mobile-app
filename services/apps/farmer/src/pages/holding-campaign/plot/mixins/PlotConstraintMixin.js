export default {

  methods: {
    constraintCaptionText (constraint) {
      switch (constraint.name) {
        case 'surface_water':
        case 'water_course':
          return this.$gettext('Water bodies and courses')
        case 'protected_site':
          return this.$gettext('Protected sites (Natura2000, ...)')
        case 'management_restriction_or_regulation_zone':
          return this.$gettext('Zone vulnerable to nitrate contamination')
        default:
          return ''
      }
    },
    constraintDescription (constraint) {
      return Array.isArray(constraint.description) ? constraint.description : [constraint.description]
    },
    constraintIconName (constraint) {
      switch (constraint.name) {
        case 'surface_water':
        case 'water_course':
          return 'las la-water'
        case 'protected_site':
          return 'las la-tree'
        case 'management_restriction_or_regulation_zone':
          return 'las la-balance-scale-right'
        default:
          return ''
      }
    },
    constraintText (constraint) {
      let text = ''
      const description = constraint.description
      const constraintFeatureName = this.constraintFeatureName(constraint)

      switch (constraint.name) {
        case 'surface_water':
        case 'water_course':
          text += `${this.getWaterDistanceDisplay(constraint)}${this.$pgettext('meter (unit)', 'm')} `
          if (description[constraint.name + '_name']) {
            text += `${this.$pgettext('X meters from "River Name"', 'from')} ${constraintFeatureName}`
          } else {
            if (constraint.name === 'surface_water') {
              text += `${this.$pgettext('X meters from a water surface', 'from a water surface')}`
            } else {
              text += `${this.$pgettext('X meters from a water course', 'from a water course')}`
            }
          }
          return text
        case 'protected_site':
          text += `${this.getZonePercentageDisplay(constraint)}% `
          if (constraintFeatureName) {
            text += `${this.$pgettext('X % of the plot is in zone A', 'in zone')} `
            text += constraintFeatureName
          } else {
            text += `${this.$pgettext('X % of the plot is in a protected zone', 'in a protected zone')}`
          }
          return text
        case 'management_restriction_or_regulation_zone':
          text += `${this.getZonePercentageDisplay(constraint)}% `
          if (constraintFeatureName) {
            text += `${this.$pgettext('X % of the plot is in zone A', 'in zone')} `
            text += constraintFeatureName
          } else {
            text += `${this.$pgettext('X % of the plot is in a regulated zone', 'in a regulated zone')}`
          }
          return text
        default:
          return text
      }
    },
    constraintFeatureId (constraint) {
      const featureIdKey = constraint.name + '_id'
      return constraint.description?.[featureIdKey]
    },
    constraintFeatureName (constraint) {
      const featureNameKey = constraint.name + '_name'
      return constraint.description?.[featureNameKey]?.toUpperCase()
    },
    getWaterDistanceDisplay (constraint) {
      const distance = constraint.description['distance_to_' + constraint.name]
      if (distance < 1) {
        return '< 1'
      } else if (distance < 5) {
        return distance.toFixed(1)
      } else {
        return String(Math.floor(distance))
      }
    },
    getZonePercentageDisplay (constraint) {
      let percentage = constraint.description['plot_pct_in_' + constraint.name]
      percentage = Math.ceil(percentage * 100)
      percentage = Math.max(Math.min(percentage, 100), 0)
      return percentage
    }
  }

}
