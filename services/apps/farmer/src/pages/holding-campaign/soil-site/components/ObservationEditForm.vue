<template>
  <q-card
    :square="!isDesktopPlatform"
    :class="{
      'observation-edit-form-desktop': isDesktopPlatform,
      'observation-edit-form-mobile': !isDesktopPlatform,
    }"
  >
    <q-form @submit="onSubmit">
      <q-card-section class="q-pb-xs">
        <q-select
          v-model="selectedObservableProperty"
          :options="availableObservableProperties"
          option-value="id"
          option-label="labelI18n"
          filled
          :loading="$apollo.queries.observableProperties.loading"
          :label="$gettext('Soil analysis')"
          :rules="[
            (val) => Boolean(val) || $gettext('Select a soil analysis')
          ]"
          @update:model-value="onObservablePropertyChange"
        />
      </q-card-section>

      <q-card-section
        class="q-py-xs"
        v-if="selectedObservableProperty"
      >
        <q-input
          v-if="selectedObservablePropertyType === 'numeric'"
          v-model="value"
          filled
          :suffix="valueSuffix"
          :label="$gettext('Value')"
          :rules="selectedObservablePropertyNumericRules"
        />
        <q-input
          v-else-if="selectedObservablePropertyType === 'text'"
          v-model="value"
          filled
          :suffix="valueSuffix"
          :label="$gettext('Value')"
        />
        <q-select
          v-else-if="selectedObservablePropertyType === 'category'"
          v-model="value"
          filled
          option-value="value"
          option-label="label"
          :options="selectedObservablePropertyChoices"
          :label="$gettext('Value')"
        />
        <q-btn-toggle
          v-else-if="selectedObservablePropertyType === 'boolean'"
          v-model="value"
          toggle-color="primary"
          :options="[
            { label: $gettext('Yes'), value: true },
            { label: $gettext('No'), value: false },
          ]"
        />
      </q-card-section>

      <q-card-section class="q-py-lg">
        <div class="row justify-end items-center no-wrap">
          <q-btn
            :label="$gettext('Cancel')"
            flat
            @click="onCancel"
          />
          <q-btn
            :label="$gettext('Remove')"
            flat
            color="red"
            @click="onDelete"
            v-if="!isNew"
          />
          <q-btn
            :label="saveButtonLabel"
            flat
            color="primary"
            type="submit"
          />
        </div>
      </q-card-section>
    </q-form>
  </q-card>
</template>

<script>
import { PlatformMixin } from 'src/mixins/PlatformMixin'
import queryObservableProperties from 'src/pages/holding-campaign/soil-site/graphql/queryObservableProperties'
import { getI18nField } from 'src/services/translate'

export default {
  name: 'ObservationEditForm',
  mixins: [PlatformMixin],
  props: {
    id: {
      type: Number,
      default: null
    },
    initialObservableProperty: {
      type: Object,
      default: null
    },
    initialValue: {
      type: String,
      default: null
    },
    existingObservations: {
      type: Array,
      required: true
    }
  },
  emits: ['on-save', 'on-cancel', 'on-delete'],
  data () {
    return {
      selectedObservableProperty: this.initialObservableProperty,
      value: this.initialValue,
      observablePropertiesFilterValue: null
    }
  },
  apollo: {
    observableProperties: {
      query: queryObservableProperties,
      notifyOnNetworkStatusChange: true
    }
  },
  computed: {
    isNew () {
      return !this.id
    },
    availableObservableProperties () {
      if (this.observableProperties) {
        return this.observableProperties.filter(
          (observableProperty) =>
            !this.existingObservations?.find(
              o => o?.observableProperty?.id === observableProperty.id
            )
        ).map(observableProperty => ({
          ...observableProperty,
          labelI18n: getI18nField(observableProperty, 'label')
        }))
      } else {
        return []
      }
    },
    saveButtonLabel () {
      return this.isNew ? this.$gettext('Add') : this.$gettext('Update')
    },
    valueSuffix () {
      const suffix = getI18nField(this.selectedObservableProperty?.unitOfMeasure, 'symbol')
      const unitOfMeasureId = this.selectedObservableProperty?.unitOfMeasure?.id
      if (suffix === '-' || unitOfMeasureId === 'no_unit') {
        return null
      } else {
        return suffix
      }
    },
    selectedObservablePropertyType () {
      return this.selectedObservableProperty?.validationRules?.type || 'text'
    },
    selectedObservablePropertyMin () {
      return this.selectedObservablePropertyType === 'numeric' ? this.selectedObservableProperty?.validationRules?.min : null
    },
    selectedObservablePropertyMax () {
      return this.selectedObservablePropertyType === 'numeric' ? this.selectedObservableProperty?.validationRules?.max : null
    },
    selectedObservablePropertyChoices () {
      if (this.selectedObservablePropertyType === 'category') {
        const choices = this.selectedObservableProperty?.validationRules?.choices || []
        return [...choices].sort((a, b) => a.label.localeCompare(b.label))
      } else {
        return []
      }
    },
    selectedObservablePropertyNumericRules () {
      if (this.selectedObservablePropertyType === 'numeric') {
        return [
          (val) =>
            (val !== null && val.trim() !== '') || this.$gettext('Enter a value'),
          (val) =>
            !isNaN(val.replace(',', '.')) || this.$gettext('Value must be a number'),
          (val) =>
            this.selectedObservablePropertyMin == null ||
            parseFloat(val.replace(',', '.')) >= this.selectedObservablePropertyMin ||
            this.$gettextInterpolate(this.$gettext('Value must be greater than or equal to %{ min }'), {
              min: this.selectedObservablePropertyMin
            }),
          (val) =>
            this.selectedObservablePropertyMax == null ||
            parseFloat(val.replace(',', '.')) <= this.selectedObservablePropertyMax ||
            this.$gettextInterpolate(this.$gettext('Value must be less than or equal to %{ max }'), {
              max: this.selectedObservablePropertyMax
            })
        ]
      } else {
        return []
      }
    }
  },
  methods: {
    onSubmit () {
      if (this.selectedObservableProperty && this.value !== null) {
        let result = {}
        if (this.selectedObservablePropertyType === 'numeric') {
          //! since input type is not number, we must handle the comma separator
          result.value = parseFloat(this.value.replace(',', '.'))
        } else if (this.selectedObservablePropertyType === 'boolean') {
          result.value = Boolean(this.value)
        } else if (this.selectedObservablePropertyType === 'category') {
          result = { ...this.value }
        } else {
          result.value = this.value.trim()
        }
        this.$emit('on-save', {
          id: this.id,
          observableProperty: this.selectedObservableProperty,
          result
        })
      }
    },
    onCancel () {
      this.$emit('on-cancel')
    },
    onDelete () {
      this.$emit('on-delete', this.id)
    },
    onObservablePropertyChange () {
      this.value = null
    }
  }
}
</script>

<style scoped>
.observation-edit-form-desktop {
  min-width: 500px;
}
.observation-edit-form-mobile {
  min-width: 270px;
}
</style>
