import queryFertilicalcNPK from 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/graphql/queryFertilicalcNPK'
import { calculateNQuantityFromIrrigation } from 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/mixins/FertilicalcMixin'

export default {
  data () {
    return {
      computing: false
    }
  },
  methods: {
    async computeNPKRecommendation () {
      this.computing = true
      try {
        const inputs = []
        const plots = [this.$store.state.fertilizationPlan.target]
        plots.forEach(p => {
          const soilObject = this.$store.state.fertilizationPlan.parameters.soil_objects?.find(so => so.plot.id === p.id)
          const isStrategyMaintenance = this.$store.state.fertilizationPlan?.parameters?.strategy?.value === 'MAINTENANCE'
          inputs.push({
            rotations: [
              {
                index: 0,
                profile: this.$store.state.fertilizationPlan.parameters.previous_plant_species_profile.id,
                plant_species_group: this.$store.state.fertilizationPlan.parameters.previous_plant_species.plant_species_group.id,
                yield: this.$store.state.fertilizationPlan.parameters.previous_yield,
                f_residues: this.$store.state.fertilizationPlan.parameters.previous_fraction_residues
              },
              {
                index: 1,
                profile: this.$store.state.fertilizationPlan.parameters.plant_species_profile.id,
                plant_species_group: this.$store.state.fertilizationPlan.parameters.plant_species.plant_species_group.id,
                yield: this.$store.state.fertilizationPlan.parameters.yield,
                f_residues: this.$store.state.fertilizationPlan.parameters.current_fraction_residues
              }
            ],
            soil: {
              // in the case where the strategy is MAINTENANCE add default values for soil data
              // default values are necessary for the graphql request but not taken into account in the algo
              soil_type: soilObject.observations.texture.value,
              som: isStrategyMaintenance ? null : soilObject.observations.organic_matter_pct.value,
              p: isStrategyMaintenance ? 0 : soilObject.observations.phosphorus.value,
              k: isStrategyMaintenance ? 0 : soilObject.observations.potassium.value
            },
            strategy: this.$store.state.fertilizationPlan.parameters.strategy.value,
            tillage: this.$store.state.fertilizationPlan.parameters.previous_incorporate_residues_with_tillage,
            plot_id: p.id
          })
        })

        const results = await this.$apollo.query({
          query: queryFertilicalcNPK,
          variables: { inputs },
          fetchPolicy: 'no-cache'
        })

        // Re-group result per plot and keep only the second (current) rotation
        const filteredResults = results.data.fertilicalc__npk
          .filter(npk => npk.rotation_index === 1)
          .map(npk => {
            const plot = this.$store.state.fertilizationPlan.target
            if (!plot) {
              throw Error(`Wrong plot_id received from Fertilicalc: ${npk.plot_id}`)
            }
            const NPKRecommendation = { ...npk }
            // review the n recommendation if there is nitrate in water or nitrate in soil
            if (this.$store.state.fertilizationPlan.parameters?.nitrogen_in_soil) {
              NPKRecommendation.n = Math.max(0, NPKRecommendation.n - Number(this.$store.state.fertilizationPlan.parameters.nitrogen_in_soil))
            }
            if (this.$store.state.fertilizationPlan.parameters?.irrigation_method?.value === 'IRRIGATED') {
              NPKRecommendation.n = Math.max(0, NPKRecommendation.n - calculateNQuantityFromIrrigation(this.$store.state.fertilizationPlan.parameters.nitrogen_in_water, this.$store.state.fertilizationPlan.parameters.irrigation_dotation))
            }
            return {
              plot,
              ...NPKRecommendation,
              initialNPKRecommendation: { n: npk.n, p: npk.p, k: npk.k }
            }
          })
        this.computing = false
        return filteredResults
      } catch (e) {
        console.error(e)
        this.$q.notify({
          color: 'red',
          message: this.$gettext('There was an error while computing the plan.')
        })
        this.computing = false
        return null
      }
    }
  }
}
