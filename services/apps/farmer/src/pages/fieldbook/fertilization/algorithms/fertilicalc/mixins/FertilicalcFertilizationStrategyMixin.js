export default {
  computed: {
    basalApplicationNPKNeeds () {
      // deduce the npk needs based on the npk results, the basal share (N share) percentage (only if there is
      // at least one topdressing application), and the manures (if applied).
      const hasTopdressing = Boolean(this.$store.state.fertilizationPlan.parameters.fertilization_strategy_has_topdressing_application)
      // basal share applies only for N
      const basalShare = hasTopdressing ? Number(this.$store.state.fertilizationPlan.parameters.fertilization_strategy_n_share) : 100
      const needs = {
        n: Math.max(Number(((Number(this.cropNPKNeeds.n) * basalShare / 100) - Number(this.NPKProvidedByManure.n)).toFixed(1)), 0),
        p: Math.max(Number((Number(this.cropNPKNeeds.p) - Number(this.NPKProvidedByManure.p)).toFixed(1)), 0),
        k: Math.max(Number((Number(this.cropNPKNeeds.k) - Number(this.NPKProvidedByManure.k)).toFixed(1)), 0)
      }
      return needs
    },
    cropNPKNeeds () {
      const res = this.$store.state.fertilizationPlan.parameters.results[0]
      return {
        n: Number(res.n.toFixed(1)),
        p: Number(res.p.toFixed(1)),
        k: Number(res.k.toFixed(1))
      }
    },
    NPKProvidedByManure () {
      const providedByManure = {
        n: 0,
        p: 0,
        k: 0
      }
      if (this.$store.state.fertilizationPlan.parameters.fertilization_strategy_has_manure_application) {
        this.$store.state.fertilizationPlan.parameters.fertilization_strategy_manure_applications.forEach(manureApplication => {
          providedByManure.n += Number(manureApplication.n_supplied)
          providedByManure.p += Number(manureApplication.p_supplied)
          providedByManure.k += Number(manureApplication.k_supplied)
        })
        providedByManure.n = Number(providedByManure.n.toFixed(1))
        providedByManure.p = Number(providedByManure.p.toFixed(1))
        providedByManure.k = Number(providedByManure.k.toFixed(1))
      }
      return providedByManure
    }
  }
}
