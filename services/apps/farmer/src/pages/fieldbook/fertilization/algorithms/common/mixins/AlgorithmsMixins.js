export default {
  methods: {
    updateParameter (key, value) {
      const newParameters = { ...this.$store.state.fertilizationPlan.parameters }
      newParameters[key] = value
      this.$store.commit('fertilizationPlan/setParameters', newParameters)
    },
    isParameterSet (storeParameter) {
      return storeParameter !== null &&
        storeParameter !== undefined &&
        storeParameter !== '' &&
        !(typeof storeParameter === 'object' && !(storeParameter instanceof Date) && Object.keys(storeParameter).length === 0)
    }
  }
}
