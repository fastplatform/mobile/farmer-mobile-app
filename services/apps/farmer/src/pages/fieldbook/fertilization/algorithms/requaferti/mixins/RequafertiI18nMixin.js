import { getCurrentLanguage, getI18nFieldName } from 'src/services/translate'
import algorithms, { REQUAFERTI } from 'src/services/fieldbook/fertilization/algorithms'

const requafertI18nLanguages = algorithms[REQUAFERTI].i18nLanguages

export default {
  methods: {
    getRequafertiI18NFieldName (fieldName) {
      if (requafertI18nLanguages.includes(getCurrentLanguage())) {
        return getI18nFieldName(fieldName)
      }
      return fieldName
    }
  }
}
