import mutationUpdateFertilizationPlan from 'src/pages/fieldbook/fertilization/graphql/mutationUpdateFertilizationPlan'
import mutationInsertFertilizationPlan from 'src/pages/fieldbook/fertilization/graphql/mutationInsertFertilizationPlan'
import {
  chemicalElementsIds as _chemicalElementsIds,
  unitOfMeasures as _unitOfMeasures,
  unitOfMeasuresSymbol as _unitOfMeasuresSymbol,
  resultTypes as _resultTypes
} from 'src/services/fieldbook/fertilization/fertilizationPlan'

export default {
  data () {
    return {
      chemicalElementsIds: _chemicalElementsIds,
      unitOfMeasures: _unitOfMeasures,
      unitOfMeasuresSymbol: _unitOfMeasuresSymbol,
      resultTypes: _resultTypes,
      newPlanName: null,
      saving: false
    }
  },
  computed: {
    isNew () {
      return !this.$store.state.fertilizationPlan?.fertilizationPlanIdAndName?.id
    },
    savePlanButtonLabel () {
      return this.isNew ? this.$gettext('Save plan') : this.$gettext('Update plan')
    }
  },
  methods: {
    async saveFertilizationPlan (results) {
      // Will save the fertilization plan currently in the fertilizationPlan object of the store state
      if (!this.isNew) {
        // It's an update
        const updateResult = await this.$apollo.mutate({
          mutation: mutationUpdateFertilizationPlan,
          variables: {
            fertilization_plan_id: this.$store.state.fertilizationPlan.fertilizationPlanIdAndName.id,
            algorithm_id: this.$store.state.fertilizationPlan.algorithm.id,
            plot_id: this.$store.state.fertilizationPlan.target.id,
            name: this.$store.state.fertilizationPlan.fertilizationPlanIdAndName.name,
            parameters: this.$store.state.fertilizationPlan.parameters,
            updated_at: new Date(),
            updated_by_id: this.$store.state.fastplatform.user.username,
            results
          }
        })
        this.$store.commit('fertilizationPlan/setFertilizationPlanIdAndName', updateResult.data.update_fertilization_plan_by_pk)
        this.$q.notify({
          color: 'green',
          message: this.$gettext('The plan has been successfully updated.')
        })
      } else {
        // It's an insert
        const insertResult = await this.$apollo.mutate({
          mutation: mutationInsertFertilizationPlan,
          variables: {
            algorithm_id: this.$store.state.fertilizationPlan.algorithm.id,
            plot_id: this.$store.state.fertilizationPlan.target.id,
            name: this.newPlanName || this.$store.state.fertilizationPlan.target.name,
            parameters: this.$store.state.fertilizationPlan.parameters,
            updated_at: new Date(),
            updated_by_id: this.$store.state.fastplatform.user.username,
            results
          }
        })
        this.$store.commit('fertilizationPlan/setFertilizationPlanIdAndName', { ...insertResult.data.insert_fertilization_plan_one })
        this.$q.notify({
          color: 'green',
          message: this.$gettext('The plan has been successfully saved.')
        })
      }
    },
    async savePlan (results) {
      this.saving = true

      // new plan: name must be confirmed
      if (this.isNew) {
        this.confirmPlanName(
          () => { this.doSavePlan(results) },
          () => { this.saving = false }
        )
      } else {
        // existing plan
        this.doSavePlan(results)
      }
    },
    async doSavePlan (results) {
      try {
        await this.saveFertilizationPlan(results)
        // redirect to the fertilization plans page
        this.$router.push({ name: 'fertilizationPlans' })
      } catch (e) {
        console.error('Could not save fertilization plan', e)
        this.$q.notify({
          color: 'red',
          message: this.$gettext('There was an error while saving the plan. Please try again.')
        })
      } finally {
        this.saving = false
      }
    },
    confirmPlanName (onOk, onCancel) {
      const plotName = this.$store.state.fertilizationPlan.target.name
      this.newPlanName = `${this.$gettext('Plan')} - ${plotName}`
      this.$q.dialog({
        title: this.$gettext('Save plan'),
        message: this.$gettext('Enter a name:'),
        prompt: {
          model: this.newPlanName,
          isValid: val => val && val.length > 3,
          type: 'text'
        },
        cancel: true
      }).onOk(data => {
        this.newPlanName = data
        onOk()
      }).onCancel(onCancel)
    },
    roundQuantity (quantity) {
      return Math.round(quantity * 10) / 10
    },
    getResultDisplayUnit (displayUnitConfig) {
      const quantityUnitName = displayUnitConfig?.quantity?.name || 'kilogram'
      const surfaceUnitName = displayUnitConfig?.surface?.name || 'hectare'
      const displayUnit = _unitOfMeasuresSymbol[`${quantityUnitName}_per_${surfaceUnitName}`]
      console.assert(Boolean(displayUnit), 'Display unit is undefined')
      return displayUnit
    },
    convertResults (results, displayUnitConfig) {
      const quantityConversionRatio = displayUnitConfig?.quantity?.ratio_to_kilogram || 1
      const surfaceConversionRatio = displayUnitConfig?.surface?.ratio_to_hectare || 1
      const conversionRatio = quantityConversionRatio * surfaceConversionRatio
      const displayUnit = this.getResultDisplayUnit(displayUnitConfig)
      return results.map(result => ({
        ...result,
        quantity: result.quantity * conversionRatio,
        display_unit: displayUnit
      }))
    }
  }
}
