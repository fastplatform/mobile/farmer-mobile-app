import { v4 as uuidv4 } from 'uuid'

import { exportFile } from 'quasar'
import { Share } from '@capacitor/share'
import { Directory } from '@capacitor/filesystem'
import { fileWrite, fileDelete } from 'src/utils/file-system'
import slugify from 'slugify'

import { isMobileApp, isIos, isAndroid } from 'src/mixins/PlatformMixin'
import { getNPKRecommendationsByPlot, resultTypes, unitOfMeasuresSymbol } from 'src/services/fieldbook/fertilization/fertilizationPlan'
import { formatPlotAreaByUnit } from 'src/services/plot/plotFormatting'
import algorithms from 'src/services/fieldbook/fertilization/algorithms'

export default {
  data () {
    return {
      isProcessingPDF: false
    }
  },
  methods: {
    convertBase64toBlob (content, contentType = 'application/pdf') {
      const sliceSize = 512
      const byteCharacters = window.atob(content) // method which converts base64 to binary
      const byteArrays = [
      ]
      for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize)
        const byteNumbers = new Array(slice.length)
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i)
        }
        const byteArray = new Uint8Array(byteNumbers)
        byteArrays.push(byteArray)
      }
      const blob = new Blob(byteArrays, {
        type: contentType
      }) // statement which creates the blob
      return blob
    },
    async downloadPDF (algorithmId, inputs, fileName) {
      this.isProcessingPDF = true
      const apolloClient = this.$apollo

      const algorithm = algorithms[algorithmId]
      const { query, queryResponseDataKey } = algorithm.pdf

      if (query) {
        try {
          const res = await apolloClient.query({
            query,
            variables: {
              inputs
            },
            fetchPolicy: 'no-cache'
          })
          const pdfBase64 = res?.data?.[queryResponseDataKey]?.pdf
          if (pdfBase64) {
            fileName = fileName || uuidv4()
            if (!fileName.endsWith('.pdf')) {
              fileName += '.pdf'
            }
            fileName = slugify(fileName)
            const pathToFile = this.$store.state.fastplatform?.holdingCampaign?.holding?.name != null && this.$store.state.fastplatform?.holdingCampaign?.campaign?.name != null
              ? `FaST/${this.$store.state.fastplatform.holdingCampaign.holding.name}/${this.$store.state.fastplatform.holdingCampaign.campaign.name}/`
              : ''
            await this.savePDF(pdfBase64, fileName, pathToFile)
            if (isAndroid(this.$q.platform)) {
              this.$q.notify({
                color: 'green',
                message: this.$gettextInterpolate(this.$gettext('"%{fileName}" was saved under the Documents folder of your device.'), { fileName })
              })
            }
          } else {
            throw new Error('No pdf data')
          }
        } catch (e) {
          this.$q.notify({
            color: 'red',
            message: this.$gettext('There was an error while downloading the plan.') + ' ' + this.$gettext('You can still retry.')
          })
        } finally {
          this.isProcessingPDF = false
        }
      } else {
        this.$q.notify({
          color: 'red',
          message: this.$gettext('There was an error while downloading the plan.')
        })
        this.isProcessingPDF = false
        // throw new Error('Unknown algorithm id')
      }
    },
    async sendPDF (algorithmId, inputs) {
      const apolloClient = this.$apollo

      const algorithm = algorithms[algorithmId]
      const { query, queryResponseDataKey } = algorithm.pdf

      if (query) {
        try {
          this.isProcessingPDF = true
          const res = await apolloClient.query({
            query,
            variables: {
              inputs
            },
            fetchPolicy: 'no-cache'
          })
          const pdfResponse = Boolean(res?.data?.[queryResponseDataKey].pdf)
          // PDF response in an empty string when it is send by email
          if (pdfResponse !== null) {
            this.$q.notify({
              color: 'green',
              message: this.$gettext('The fertilization plans will be sent to your email address as a PDF file (this may take a few minutes).')
            })
          } else {
            throw new Error('Bad response')
          }
        } catch (e) {
          console.error('Could not send PDF', e)
          this.$q.notify({
            color: 'red',
            message: this.$gettext('There was an error while sending the plans.') + ' ' + this.$gettext('You can still retry.')
          })
        } finally {
          this.isProcessingPDF = false
        }
      } else {
        this.$q.notify({
          color: 'red',
          message: this.$gettext('There was an error while sending the plans.')
        })
        this.isProcessingPDF = false
      }
    },
    getDownloadPDFInputs (fertilizationPlans, defaultMapBaseLayerUrl) {
      return this.getGeneratePDFInputs(fertilizationPlans, defaultMapBaseLayerUrl, null)
    },
    getSendPDFInputs (fertilizationPlans, defaultMapBaseLayerUrl, sendByEmailInputs) {
      return this.getGeneratePDFInputs(fertilizationPlans, defaultMapBaseLayerUrl, sendByEmailInputs)
    },
    getGeneratePDFInputs (fertilizationPlans, defaultMapBaseLayerUrl, sendByEmailInputs) {
      if (!(fertilizationPlans instanceof Array)) {
        // it might be a single fertilization plan
        fertilizationPlans = [fertilizationPlans]
      }
      const inputs = {
        fertilization_plans: []
      }
      if (sendByEmailInputs) {
        inputs.send_by_email = true
        inputs.send_by_email_address = sendByEmailInputs.email
        inputs.send_by_email_subject = sendByEmailInputs.subject
        inputs.send_by_email_body = sendByEmailInputs.body
        inputs.send_by_email_filename = sendByEmailInputs.filename
      }
      let stringifyInputs = false
      fertilizationPlans.forEach(fertilizationPlan => {
        const algorithmId = fertilizationPlan?.algorithm?.id
        const algorithm = algorithms[algorithmId]
        if (algorithmId === 'fertilicalc') {
          const recommendationsByPlot = getNPKRecommendationsByPlot(algorithmId, fertilizationPlan?.results)
          Object.keys(recommendationsByPlot).forEach(plotId => {
            recommendationsByPlot[plotId].recommendation = {
              ...recommendationsByPlot[plotId].recommendations[0],
              basal_configuration: fertilizationPlan?.results?.basal_configuration,
              topdressing_configuration: fertilizationPlan?.results?.topdressing_configuration
            }
            recommendationsByPlot[plotId].plot_preview = {
              base_layer_url: defaultMapBaseLayerUrl,
              plot_geometry: Number(fertilizationPlan?.plot?.id) === Number(plotId) ? fertilizationPlan?.plot?.geometry : {}
            }
          })
          inputs.fertilization_plans.push({
            fertilization_plan_name: fertilizationPlan?.name,
            campaign_name: this.$store.state.fastplatform?.holdingCampaign?.campaign?.name,
            holding_name: this.$store.state.fastplatform?.holdingCampaign?.holding?.name,
            fertilization_plan_date: fertilizationPlan?.updated_at,
            recommendations_by_plot: recommendationsByPlot,
            parameters: fertilizationPlan?.parameters
          })
        } else if (algorithmId === 'vegsyst') {
          const results = fertilizationPlan?.results ?? []
          const summary = {
            irrigation_volume: 0,
            soil_mineral_n: 0,
            mineralized_n: 0,
            n_crop_uptake: 0,
            n_fertilizer_requirements: 0,
            n_fertilizer_total_amount: 0
          }
          results.forEach(result => {
            summary.irrigation_volume += result.irrigation_requirements_daily_volume ?? 0
            summary.soil_mineral_n += result.daily_soil_mineral_n ?? 0
            summary.mineralized_n += result.daily_mineralized_n ?? 0
            summary.n_crop_uptake += result.daily_crop_n_uptake ?? 0
            summary.n_fertilizer_requirements += result.daily_n_fertilizer_requirements ?? 0
          })
          const greenhouseSize = formatPlotAreaByUnit(fertilizationPlan?.parameters?.greenhouseSize ?? 0)
          summary.n_fertilizer_total_amount = Number((summary.n_fertilizer_requirements * greenhouseSize.ha).toFixed(1))
          summary.irrigation_volume = Number(summary.irrigation_volume.toFixed(1))
          summary.soil_mineral_n = Number(summary.soil_mineral_n.toFixed(1))
          summary.mineralized_n = Number(summary.mineralized_n.toFixed(1))
          summary.n_crop_uptake = Number(summary.n_crop_uptake.toFixed(1))
          summary.n_fertilizer_requirements = Number(summary.n_fertilizer_requirements.toFixed(1))
          inputs.fertilization_plans.push({
            fertilization_plan_name: fertilizationPlan?.name,
            campaign_name: this.$store.state.fastplatform?.holdingCampaign?.campaign?.name,
            holding_name: this.$store.state.fastplatform?.holdingCampaign?.holding?.name,
            fertilization_plan_date: fertilizationPlan?.updated_at,
            plot_name: fertilizationPlan?.plot?.name,
            plot_preview: {
              base_layer_url: defaultMapBaseLayerUrl,
              plot_geometry: fertilizationPlan?.plot?.geometry ?? {}
            },
            recommendations: {
              summary,
              daily_results: fertilizationPlan?.results
            },
            parameters: fertilizationPlan?.parameters
          })
        } else if (algorithmId === 'visione') {
          const recommendationsByPlot = getNPKRecommendationsByPlot(algorithmId, fertilizationPlan?.results, false)
          Object.keys(recommendationsByPlot).forEach(plotId => {
            // the number of recommendations is equal to the number of rotations defined in this plan
            recommendationsByPlot[plotId].recommendations.forEach((recommendation, index) => {
              // add element_recommendations_by_type
              // eslint-disable-next-line camelcase
              const element_recommendations_by_type = {}
              recommendation.element_recommendations.forEach(elementRecommendation => {
                if (!Object.keys(element_recommendations_by_type).includes(elementRecommendation.recommendation_type)) {
                  let recoTypeLabel = null
                  switch (elementRecommendation.recommendation_type) {
                    case resultTypes.PLANT_NEEDS:
                      recoTypeLabel = this.$gettext('Needs')
                      break
                    case resultTypes.PLANNED:
                      recoTypeLabel = this.$gettext('Provided by organic fertilizer')
                      break
                    case resultTypes.STANDARD_DOSE:
                      recoTypeLabel = this.$gettext('Standard Dose')
                      break
                    case resultTypes.RECOMMENDATION:
                      recoTypeLabel = this.$gettext('Recommendation')
                      break
                    default:
                      recoTypeLabel = elementRecommendation.recommendation_type
                      break
                  }
                  element_recommendations_by_type[elementRecommendation.recommendation_type] = {
                    recommendation_type_label: recoTypeLabel,
                    elements: {
                      n: {
                        quantity: null,
                        quantity_unit_of_measure_symbol: unitOfMeasuresSymbol.kilogram_per_hectare
                      },
                      p: {
                        quantity: null,
                        quantity_unit_of_measure_symbol: unitOfMeasuresSymbol.kilogram_per_hectare
                      },
                      k: {
                        quantity: null,
                        quantity_unit_of_measure_symbol: unitOfMeasuresSymbol.kilogram_per_hectare
                      }
                    }
                  }
                }
                element_recommendations_by_type[elementRecommendation.recommendation_type].elements[elementRecommendation.chemical_element_id.toLowerCase()].quantity = elementRecommendation.quantity
                element_recommendations_by_type[elementRecommendation.recommendation_type].elements[elementRecommendation.chemical_element_id.toLowerCase()].quantity_unit_of_measure_symbol = elementRecommendation.quantity_unit_of_measure_symbol
              })
              // use a list here to set the display order. PLANT_NEEDS => PLANNED => STANDARD_DOSE => RECOMMENDATION
              recommendationsByPlot[plotId].recommendations[index].element_recommendations_by_type = []
              const keys = Object.keys(element_recommendations_by_type)
              if (keys.includes(resultTypes.PLANT_NEEDS)) {
                recommendationsByPlot[plotId].recommendations[index].element_recommendations_by_type.push(element_recommendations_by_type[resultTypes.PLANT_NEEDS])
              }
              if (keys.includes(resultTypes.PLANNED)) {
                recommendationsByPlot[plotId].recommendations[index].element_recommendations_by_type.push(element_recommendations_by_type[resultTypes.PLANNED])
              }
              if (keys.includes(resultTypes.STANDARD_DOSE)) {
                recommendationsByPlot[plotId].recommendations[index].element_recommendations_by_type.push(element_recommendations_by_type[resultTypes.STANDARD_DOSE])
              }
              if (keys.includes(resultTypes.RECOMMENDATION)) {
                recommendationsByPlot[plotId].recommendations[index].element_recommendations_by_type.push(element_recommendations_by_type[resultTypes.RECOMMENDATION])
              }
            })
            recommendationsByPlot[plotId].plot_preview = {
              base_layer_url: defaultMapBaseLayerUrl,
              plot_geometry: Number(fertilizationPlan?.plot?.id) === Number(plotId) ? fertilizationPlan?.plot?.geometry : {}
            }
          })
          inputs.fertilization_plans.push({
            fertilization_plan_name: fertilizationPlan?.name,
            campaign_name: this.$store.state.fastplatform?.holdingCampaign?.campaign?.name,
            holding_name: this.$store.state.fastplatform?.holdingCampaign?.holding?.name,
            fertilization_plan_date: fertilizationPlan?.updated_at,
            recommendations_by_plot: recommendationsByPlot,
            parameters: fertilizationPlan?.parameters
          })
        } else if (algorithmId === 'arc') {
          const recommendationsByPlot = getNPKRecommendationsByPlot(algorithmId, fertilizationPlan?.results)
          Object.keys(recommendationsByPlot).forEach(plotId => {
            recommendationsByPlot[plotId].recommendation = {
              ...recommendationsByPlot[plotId].recommendations[0]
            }
            recommendationsByPlot[plotId].plot_preview = {
              base_layer_url: defaultMapBaseLayerUrl,
              plot_geometry: Number(fertilizationPlan?.plot?.id) === Number(plotId) ? fertilizationPlan?.plot?.geometry : {}
            }
          })
          inputs.fertilization_plans.push({
            fertilization_plan_name: fertilizationPlan?.name,
            campaign_name: this.$store.state.fastplatform?.holdingCampaign?.campaign?.name,
            holding_name: this.$store.state.fastplatform?.holdingCampaign?.holding?.name,
            fertilization_plan_date: fertilizationPlan?.updated_at,
            recommendations_by_plot: recommendationsByPlot,
            parameters: fertilizationPlan?.parameters
          })
          stringifyInputs = true
        } else if (algorithm?.hasNewResultFormat && algorithm.pdf) {
          // 'New algorithms' such as Requaferti, NavigatorF3, HK...
          const recommendationsByPlot = getNPKRecommendationsByPlot(algorithmId, fertilizationPlan?.results, false)
          Object.keys(recommendationsByPlot).forEach(plotId => {
            recommendationsByPlot[plotId].plot_preview = {
              base_layer_url: defaultMapBaseLayerUrl,
              plot_geometry: Number(fertilizationPlan?.plot?.id) === Number(plotId) ? fertilizationPlan?.plot?.geometry : {}
            }
          })
          inputs.fertilization_plans.push({
            fertilization_plan_name: fertilizationPlan?.name,
            campaign_name: this.$store.state.fastplatform?.holdingCampaign?.campaign?.name,
            holding_name: this.$store.state.fastplatform?.holdingCampaign?.holding?.name,
            fertilization_plan_date: fertilizationPlan?.updated_at,
            recommendations_by_plot: recommendationsByPlot,
            parameters: fertilizationPlan?.parameters
          })
          stringifyInputs = true
        } else {
          throw new Error(`Algorithm  ${algorithmId} does not support PDF export`)
        }
      })
      if (inputs.fertilization_plans.length) {
        if (stringifyInputs) {
          console.log('PDF inputs', inputs)
          return JSON.stringify(inputs)
        } else {
          return inputs
        }
      } else {
        return null
      }
    },
    async savePDF (pdfBase64, fileName, pathToFile = '/') {
      if (!fileName.endsWith('.pdf')) fileName += '.pdf'
      fileName = slugify(fileName)
      if (!pathToFile.endsWith('/')) pathToFile += '/'
      if (isMobileApp(this.$q.platform)) {
        if (!pdfBase64.startsWith('data:application/pdf;base64,')) {
          pdfBase64 = 'data:application/pdf;base64,' + pdfBase64
        }
        try {
          const filePath = 'FaST/' + pathToFile + fileName
          const res = await fileWrite({
            data: pdfBase64,
            path: filePath,
            directory: Directory.Documents,
            recursive: true
          })
          if (isIos(this.$q.platform)) {
            await Share.share({
              title: fileName,
              url: res.uri,
              dialogTitle: fileName
            })
            await fileDelete(filePath, Directory.Documents)
          }
        } catch (e) {
          console.error('Failed to save PDF', e)
          throw new Error(e)
        }
      } else {
        const blob = this.convertBase64toBlob(pdfBase64)
        const status = exportFile(fileName, blob, 'application/pdf')
        if (status !== true) {
          // browser denied it
          // open the pdf in a new tab and let the browser display it
          const pdf = new File([blob], fileName, { type: 'application/pdf', lastModified: new Date() })
          const pdfURL = URL.createObjectURL(pdf)
          window.open(pdfURL)
          URL.revokeObjectURL(pdfURL)
        }
      }
    }
  }
}
