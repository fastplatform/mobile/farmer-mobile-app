import featureFlags from 'src/services/feature-flags'

export default [
  {
    path: '/holdings',
    name: 'holdings',
    component: () => import(/* webpackChunkName: "holdings" */ 'pages/holdings/HoldingsPage'),
    meta: {
      footerGroupName: 'holdings',
      isAvailableWithoutHoldingCampaign: true
    }
  },
  {
    path: '/holdings-sync',
    name: 'holdingsSync',
    component: () => import(/* webpackChunkName: "holdings" */ 'pages/holdings/HoldingsSyncPage'),
    meta: {
      footerGroupName: 'holdings',
      isAvailableWithoutHoldingCampaign: true
    },
    beforeEnter: (to, from, next) => {
      if (featureFlags.canSynchronizeHoldingAndHoldings()) {
        next()
      } else {
        next(false)
      }
    }
  },
  {
    path: '/holding-campaign-sync/:holdingId/:campaignId',
    name: 'holdingCampaignSync',
    component: () => import(/* webpackChunkName: "holdings" */ 'pages/holdings/HoldingCampaignSyncPage'),
    props: true,
    meta: {
      footerGroupName: 'holdings',
      isAvailableWithoutHoldingCampaign: true
    },
    beforeEnter: (to, from, next) => {
      if (featureFlags.canSynchronizeHoldingAndHoldings()) {
        next()
      } else {
        next(false)
      }
    }
  },
  {
    path: '/holding-campaign-duplication/:holdingId/:campaignId',
    name: 'holdingCampaignDuplication',
    component: () => import(/* webpackChunkName: "holdings" */ 'pages/holdings/HoldingCampaignDuplicationPage'),
    props: true,
    meta: {
      footerGroupName: 'holdings'
    }
  },
  {
    path: '/holding-demo-creation/:campaignId',
    name: 'holdingDemoCreation',
    component: () => import(/* webpackChunkName: "holdings" */ 'pages/holdings/HoldingDemoCreationPage'),
    props: true,
    meta: {
      footerGroupName: 'holdings',
      isAvailableWithoutHoldingCampaign: true
    }
  }
]
