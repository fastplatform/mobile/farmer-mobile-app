export default [
  {
    path: '/add-ons',
    name: 'addOns',
    component: () => import(/* webpackChunkName: "addOns" */ 'pages/add-ons/AddOnsPage'),
    meta: {
      footerGroupName: 'addOns'
    }
  },
  {
    path: '/add-on/:addOnId',
    name: 'addOn',
    component: () => import(/* webpackChunkName: "addOns" */ 'pages/add-ons/AddOnPage'),
    meta: {
      footerGroupName: 'addOns'
    }
  }
]
