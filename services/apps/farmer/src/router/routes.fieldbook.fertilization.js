export default [
  {
    path: '/fieldbook/fertilization/fertilization-plans',
    name: 'fertilizationPlans',
    component: () => import(/* webpackChunkName: "fertilization" */ 'pages/fieldbook/fertilization/FertilizationPlansPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/plotSelection',
    name: 'fertilizationPlanPlotSelection',
    component: () => import(/* webpackChunkName: "fertilization" */ 'pages/fieldbook/fertilization/algorithms/common/PlotSelectionPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/:fertilizationPlanId',
    name: 'fertilizationPlan',
    component: () => import(/* webpackChunkName: "fertilization" */ 'pages/fieldbook/fertilization/FertilizationPlanPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
