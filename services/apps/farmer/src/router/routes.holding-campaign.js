import featureFlags from 'src/services/feature-flags'
export default [
  {
    path: '/holding-campaign',
    name: 'holdingCampaign',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/holding-campaign/HoldingCampaignPage'),
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/plots',
    name: 'plots',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/plot/PlotsPage'),
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/plots/new',
    name: 'plotAdd',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/plot/PlotAddPage'),
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/plots/:agriPlotId/new',
    name: 'plotCreate',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/plot/PlotCreationPage'),
    beforeEnter: (to, from, next) => {
      if (featureFlags.plotCanBeAdded()) {
        next()
      } else {
        if (from.name === null) {
          next({ path: '/' })
        } else {
          next(false)
        }
      }
    },
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/plots/:idPlot',
    name: 'plot',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/plot/PlotPage'),
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/plots/:idPlot/edit-crops/:idCrop?',
    name: 'plotCropEdit',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/plot/PlotCropEditPage'),
    beforeEnter: (to, from, next) => {
      if ((featureFlags.plotPlantVarietyCanBeAdded() && !to.params?.idCrop) || (featureFlags.plotPlantVarietyCanBeEdited() && to.params?.idCrop)) {
        next()
      } else {
        if (from.name === null) {
          next({ path: '/' })
        } else {
          next(false)
        }
      }
    },
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/plot-constraints/:constraintId',
    name: 'plotConstraint',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/plot/PlotConstraintPage'),
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/soil-sites/new/?:plotId?',
    name: 'holdingSoilSiteCreate',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/soil-site/SoilSiteEditPage'),
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/soil-sites/:soilSiteId',
    name: 'holdingSoilSiteEdit',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/soil-site/SoilSiteEditPage'),
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/custom-fertilizers/new',
    name: 'holdingCustomFertilizerCreate',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/custom-fertilizer/CustomFertilizerEditPage'),
    beforeEnter: (to, from, next) => {
      if (featureFlags.canCreateAndEditCustomFertilizer()) {
        next()
      } else {
        next(false)
      }
    },
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  },
  {
    path: '/holding-campaign/custom-fertilizers/:customFertilizerId',
    name: 'holdingCustomFertilizerEdit',
    component: () => import(/* webpackChunkName: "holding" */ 'pages/holding-campaign/custom-fertilizer/CustomFertilizerEditPage'),
    beforeEnter: (to, from, next) => {
      if (featureFlags.canCreateAndEditCustomFertilizer()) {
        next()
      } else {
        next(false)
      }
    },
    meta: {
      footerGroupName: 'holdingCampaign'
    }
  }
]
