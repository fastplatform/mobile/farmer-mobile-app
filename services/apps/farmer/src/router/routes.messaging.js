import featureFlags from 'src/services/feature-flags'

export default [
  {
    path: '/messaging',
    name: 'messaging',
    component: () => import(/* webpackChunkName: "holdings" */ 'pages/messaging/MessagingPage'),
    meta: {
      isAvailableWithoutHoldingCampaign: true
    }
  },
  {
    path: '/messaging/:messageType/:messageId',
    name: 'message',
    component: () => import(/* webpackChunkName: "message" */ 'pages/messaging/MessagePage'),
    meta: {
      isAvailableWithoutHoldingCampaign: true
    }
  },
  {
    path: '/messaging/create',
    name: 'messageCreation',
    component: () => import(/* webpackChunkName: "message" */ 'pages/messaging/MessageCreationPage'),
    meta: {
      isAvailableWithoutHoldingCampaign: true
    },
    beforeEnter: (to, from, next) => {
      if (featureFlags.canCreateTicket()) {
        next()
      } else {
        next(false)
      }
    }
  }
]
