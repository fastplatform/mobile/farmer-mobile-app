export default [
  {
    path: '/fieldbook/fertilization/fertilization-plan/kalkulacky-hnojenie/crop-profile',
    name: 'kalkulackyHnojenieCropProfile',
    component: () => import(/* webpackChunkName: "kalkulacky-hnojenie" */ 'src/pages/fieldbook/fertilization/algorithms/kalkulacky-hnojenie/pages/KalkulackyHnojenieCropProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/kalkulacky-hnojenie/soil',
    name: 'kalkulackyHnojenieSoil',
    component: () => import(/* webpackChunkName: "kalkulacky-hnojenie" */ 'src/pages/fieldbook/fertilization/algorithms/kalkulacky-hnojenie/pages/KalkulackyHnojenieSoilPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/kalkulacky-hnojenie/nvz',
    name: 'kalkulackyHnojenieNVZ',
    component: () => import(/* webpackChunkName: "kalkulacky-hnojenie" */ 'src/pages/fieldbook/fertilization/algorithms/kalkulacky-hnojenie/pages/KalkulackyHnojenieNVZPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/kalkulacky-hnojenie/nutrient-requirements',
    name: 'kalkulackyHnojenieNutrientRequirements',
    component: () => import(/* webpackChunkName: "kalkulacky-hnojenie" */ 'src/pages/fieldbook/fertilization/algorithms/kalkulacky-hnojenie/pages/KalkulackyHnojenieNutrientRequirementsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/kalkulacky-hnojenie/organic-fertilization',
    name: 'kalkulackyHnojenieOrganicFertilization',
    component: () => import(/* webpackChunkName: "kalkulacky-hnojenie" */ 'src/pages/fieldbook/fertilization/algorithms/kalkulacky-hnojenie/pages/KalkulackyHnojenieOrganicFertilizationPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/kalkulacky-hnojenie/mineral-fertilization',
    name: 'kalkulackyHnojenieMineralFertilization',
    component: () => import(/* webpackChunkName: "kalkulacky-hnojenie" */ 'src/pages/fieldbook/fertilization/algorithms/kalkulacky-hnojenie/pages/KalkulackyHnojenieMineralFertilizationPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
