export default [
  {
    path: '/support',
    name: 'support',
    component: () => import(/* webpackChunkName: "support" */ 'pages/support/SupportPage'),
    meta: {
      isAvailableWithoutHoldingCampaign: true
    }
  }
]
