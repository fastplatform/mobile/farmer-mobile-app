export default [
  {
    path: '/logout',
    name: 'logout',
    component: () => import(/* webpackChunkName: "logout" */ 'pages/logout/LogoutPage'),
    meta: {
      isPublic: true
    }
  }
]
