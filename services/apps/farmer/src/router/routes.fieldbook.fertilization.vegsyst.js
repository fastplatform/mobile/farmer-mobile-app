export default [
  {
    path: '/fieldbook/fertilization/fertilization-plan/vegsyst/greenhouse',
    name: 'vegsystGreenhousePage',
    component: () => import(/* webpackChunkName: "vegsyst" */ 'src/pages/fieldbook/fertilization/algorithms/vegsyst/pages/VegsystGreenhousePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/vegsyst/crop-profile',
    name: 'vegsystCropProfilePage',
    component: () => import(/* webpackChunkName: "vegsyst" */ 'src/pages/fieldbook/fertilization/algorithms/vegsyst/pages/VegsystCropProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/vegsyst/whitening',
    name: 'vegsystWhiteningPage',
    component: () => import(/* webpackChunkName: "vegsyst" */ 'src/pages/fieldbook/fertilization/algorithms/vegsyst/pages/VegsystWhiteningPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/vegsyst/soil',
    name: 'vegsystSoilPage',
    component: () => import(/* webpackChunkName: "vegsyst" */ 'src/pages/fieldbook/fertilization/algorithms/vegsyst/pages/VegsystSoilPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/vegsyst/manure',
    name: 'vegsystManurePage',
    component: () => import(/* webpackChunkName: "vegsyst" */ 'src/pages/fieldbook/fertilization/algorithms/vegsyst/pages/VegsystManurePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/vegsyst/irrigation-layout',
    name: 'vegsystIrrigationLayoutPage',
    component: () => import(/* webpackChunkName: "vegsyst" */ 'src/pages/fieldbook/fertilization/algorithms/vegsyst/pages/VegsystIrrigationLayoutPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/vegsyst/compute',
    name: 'vegsystComputePage',
    component: () => import(/* webpackChunkName: "vegsyst" */ 'src/pages/fieldbook/fertilization/algorithms/vegsyst/pages/VegsystComputePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/vegsyst/results',
    name: 'vegsystResultsPage',
    component: () => import(/* webpackChunkName: "vegsyst" */ 'src/pages/fieldbook/fertilization/algorithms/vegsyst/pages/VegsystResultsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
