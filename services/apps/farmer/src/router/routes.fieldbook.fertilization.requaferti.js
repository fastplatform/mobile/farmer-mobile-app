export default [
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti/crop-profile',
    name: 'requafertiCropProfile',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti/pages/RequafertiCropProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti/soil',
    name: 'requafertiSoil',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti/pages/RequafertiSoilPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti/previous-crop-profile',
    name: 'requafertiPreviousCropProfile',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti/pages/RequafertiPreviousCropProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti/intermediate-crop-profile',
    name: 'requafertiIntermediateCropProfile',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti/pages/RequafertiIntermediateCropProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti/meadow-and-lucerne',
    name: 'requafertiMeadowAndLucerne',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti/pages/RequafertiMeadowAndLucernePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti/organic-manure',
    name: 'requafertiOrganicManure',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti/pages/RequafertiOrganicManurePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti/compute',
    name: 'requafertiCompute',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti/pages/RequafertiComputePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti/results',
    name: 'requafertiResults',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti/pages/RequafertiResultsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
