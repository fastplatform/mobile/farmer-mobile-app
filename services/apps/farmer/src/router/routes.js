import homeRoutes from './routes.home'
import loginRoutes from './routes.login'
import logoutRoutes from './routes.logout'
import holdingCampaignRoutes from './routes.holding-campaign'
import holdingsRoutes from './routes.holdings'
import photoRoutes from './routes.photo'
import fieldbookRoutes from './routes.fieldbook'
import fieldbookFertilizationRoutes from './routes.fieldbook.fertilization'
import fieldbookFertilizationArcRoutes from './routes.fieldbook.fertilization.arc'
import fieldbookFertilizationFertilicalcRoutes from './routes.fieldbook.fertilization.fertilicalc'
import fieldbookFertilizationVegsystRoutes from './routes.fieldbook.fertilization.vegsyst'
import fieldbookFertilizationVisioneRoutes from './routes.fieldbook.fertilization.visione'
import fieldbookFertilizationRequafertiRoutes from './routes.fieldbook.fertilization.requaferti'
import fieldbookFertilizationKalkulackyHnojenieRoutes from './routes.fieldbook.fertilization.kalkulacky-hnojenie'
import fieldbookFertilizationNavigatorF3Routes from './routes.fieldbook.fertilization.navigator-f3'
import fieldbookFertilizationIcpaPmnRoutes from './routes.fieldbook.fertilization.icpa-pmn'
import addOnsRoutes from './routes.add-ons'
import creditsRoutes from './routes.credits'
import messagingRoutes from './routes.messaging'
import supportRoutes from './routes.support'

const routes = [
  ...homeRoutes,
  ...loginRoutes,
  ...logoutRoutes,
  ...holdingCampaignRoutes,
  ...holdingsRoutes,
  ...photoRoutes,
  ...fieldbookRoutes,
  ...fieldbookFertilizationArcRoutes,
  ...fieldbookFertilizationFertilicalcRoutes,
  ...fieldbookFertilizationVegsystRoutes,
  ...fieldbookFertilizationVisioneRoutes,
  ...fieldbookFertilizationRequafertiRoutes,
  ...fieldbookFertilizationKalkulackyHnojenieRoutes,
  ...fieldbookFertilizationNavigatorF3Routes,
  ...fieldbookFertilizationIcpaPmnRoutes,
  ...fieldbookFertilizationRoutes,
  ...addOnsRoutes,
  ...creditsRoutes,
  ...messagingRoutes,
  ...supportRoutes
]

routes.push({
  path: '/:catchAll(.*)*',
  component: () => import('pages/errors/Error404Page')
})

export default routes
