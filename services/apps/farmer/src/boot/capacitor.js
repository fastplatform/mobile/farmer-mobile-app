// the camera preview is automatically registered once imported
import '@capacitor-community/camera-preview'
import { StatusBar, Style } from '@capacitor/status-bar'
import { Keyboard } from '@capacitor/keyboard'

// quasar mode (spa, capacitor, ...)
const quasarMode = process.env.MODE

export default async () => {
  // Do things only for the mobile app
  if (quasarMode === 'capacitor') {
    // set the status bar style to dark (white text)
    await StatusBar.setStyle({
      style: Style.Dark
    })
    // hide keyboard accessory bar on iOS
    try {
      await Keyboard.setAccessoryBarVisible({ isVisible: false })
    } catch (e) {
      console.warn('Could not set accessory bar visible', e)
    }
  }
}
