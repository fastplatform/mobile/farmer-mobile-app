import { Platform } from 'quasar'

export default () => {
  // lock the screen orientation to portrait mode (mobile device only)
  try {
    if (Platform.is.capacitor) {
      window.screen.orientation.lock('portrait')
    }
  } catch (e) {
    // console.error(e)
  }
}
