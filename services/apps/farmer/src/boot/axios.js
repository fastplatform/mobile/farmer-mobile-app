import axios from 'axios'

export default ({ app }) => {
  // Set the default content type for every axios request to be json
  axios.defaults.headers.post['Content-Type'] = 'application/json'

  app.config.globalProperties.$axios = axios
}
