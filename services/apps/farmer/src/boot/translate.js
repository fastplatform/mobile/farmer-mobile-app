import { createGettext } from '@jshmrtn/vue3-gettext'
import { Quasar } from 'quasar'

import translations from 'src/translate/translations.json'
import { AVAILABLE_LANGUAGES } from 'src/services/translate'

const SOURCE_LANGUAGE = 'en'
const localLanguage = Quasar.lang.getLocale().slice(0, 2)
const defaultLanguage = Object.keys(AVAILABLE_LANGUAGES).includes(localLanguage) ? localLanguage : SOURCE_LANGUAGE

const gettext = createGettext({
  availableLanguages: AVAILABLE_LANGUAGES,
  defaultLanguage,
  translations,
  mutedLanguages: [SOURCE_LANGUAGE],
  silent: true
})

export default ({ app }) => {
  app.use(gettext)
}

export const $gettext = gettext.$gettext
export const $ngettext = gettext.$ngettext
export const $npgettext = gettext.$npgettext
export const $pgettext = gettext.$pgettext
export const $interpolate = gettext.$interpolate
