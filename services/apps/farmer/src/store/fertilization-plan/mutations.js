import { initialState } from 'src/store/fertilization-plan/state'

export function setTarget (state, value) {
  state.target = value
}

export function deleteTarget (state) {
  state.target = initialState().target
}

export function setParameters (state, value) {
  state.parameters = value
}

export function deleteParameters (state) {
  state.parameters = initialState().parameters
}

export function setResults (state, value) {
  state.results = value
}

export function deleteResults (state) {
  state.results = initialState().results
}

export function setFertilizationPlanIdAndName (state, value) {
  state.fertilizationPlanIdAndName = { id: value.id, name: value.name }
}

export function deleteFertilizationPlanIdAndName (state) {
  state.fertilizationPlanIdAndName = initialState().fertilizationPlanIdAndName
}

export function setAlgorithm (state, value) {
  state.algorithm = value
}

export function deleteAlgorithm (state) {
  state.algorithm = initialState().algorithm
}

export function setIsEdition (state, value) {
  state.isEdition = value
}

export function deleteIsEdition (state) {
  state.isEdition = initialState().isEdition
}

export function setIsCreationFromPlot (state, value) {
  state.isCreationFromPlot = value
}

export function deleteIsCreationFromPlot (state) {
  state.isCreationFromPlot = initialState().isCreationFromPlot
}

export function reset (state) {
  state = initialState()
}
