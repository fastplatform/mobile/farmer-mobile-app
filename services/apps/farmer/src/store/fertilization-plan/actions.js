
export function reset (context) {
  return new Promise((resolve, reject) => {
    context.commit('deleteTarget')
    context.commit('deleteParameters')
    context.commit('deleteResults')
    context.commit('deleteFertilizationPlanIdAndName')
    context.commit('deleteAlgorithm')
    context.commit('deleteAlgorithm')
    context.commit('deleteIsEdition')
    context.commit('deleteIsCreationFromPlot')
    resolve()
  })
}
