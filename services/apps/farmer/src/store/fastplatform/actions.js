export function reset (context) {
  return new Promise((resolve, reject) => {
    // delete top level connfig
    context.commit('setTopLevelConfig', {})
    // delete config
    context.commit('deleteConfig')
    // delete user
    context.commit('deleteUser')
    // delete tokens
    context.commit('deleteTokens')
    // delete preferredLanguage
    context.commit('deletePreferredLanguage')
    // delete holdings
    context.commit('deleteHoldings')
    // delete holding campaign
    context.commit('deleteHoldingCampaign')
    // delete region
    context.commit('deleteRegion')
    // reset display surface unit
    context.commit('resetDisplaySurfaceUnit')
    // reset portal configuration
    context.commit('resetPortalConfiguration')
    // reset available languages
    context.commit('resetAvailableLanguages')
    resolve()
  })
}
