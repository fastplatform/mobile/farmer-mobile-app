import { initialState } from 'src/store/fastplatform/state'

// Holdings campaigns
export function updateHoldingCampaign (state, value) {
  state.holdingCampaign = value
}

export function deleteHoldingCampaign (state) {
  state.holdingCampaign = initialState().holdingCampaign
}

// Holdings
export function updateHoldings (state, value) {
  state.holdings = value
}

export function deleteHoldings (state) {
  state.holdings = initialState().holdings
}

// Config
export function setTopLevelConfig (state, value) {
  state.topLevelConfig = value
}

export function updateConfig (state, value) {
  state.config = value
}

export function deleteConfig (state, value) {
  state.config = initialState().config
}

// Tokens
export function updateTokens (state, value) {
  state.tokens = value
}

export function deleteTokens (state, value) {
  state.tokens = initialState().tokens
}

// User
export function updateUser (state, value) {
  state.user = value
}

export function deleteUser (state, value) {
  state.user = initialState().user
}

// Preferred language
export function updatePreferredLanguage (state, value) {
  state.preferredLanguage = value
}

export function deletePreferredLanguage (state) {
  state.preferredLanguage = initialState().preferredLanguage
}

export function updateRegion (state, value) {
  state.region = value
}

export function deleteRegion (state) {
  state.region = initialState().region
}

// Display surface unit
export function updateDisplaySurfaceUnit (state, value) {
  state.displaySurfaceUnit = value
}

export function resetDisplaySurfaceUnit (state) {
  state.displaySurfaceUnit = initialState().displaySurfaceUnit
}

// Portal configuration
export function updatePortalConfiguration (state, value) {
  state.portalConfiguration = value
}

export function resetPortalConfiguration (state) {
  state.portalConfiguration = initialState().portalConfiguration
}

// Languages
export function updateAvailableLanguages (state, value) {
  state.availableLanguages = value
}

export function resetAvailableLanguages (state) {
  state.availableLanguages = initialState().availableLanguages
}
