export const initialState = () => {
  return {
    topLevelConfig: {
      countriesAndRegionsConfigEndpoint: null,
      countriesAndRegionsConfigJSONFile: null
    },
    config: {},
    holdingCampaign: {},
    holdings: {},
    tokens: {},
    user: {},
    preferredLanguage: null,
    region: {},
    displaySurfaceUnit: null,
    portalConfiguration: {
      displaySurfaceUnit: {},
      mapOverlayStyles: {},
      mapDefaults: {}
    },
    availableLanguages: ['en']
  }
}
export default function () {
  return initialState()
}
