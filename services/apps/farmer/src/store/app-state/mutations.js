import { initialState } from 'src/store/app-state/state'

export function changePlatform (state, value) {
  state.currentPlatform = value
}

export function changeUserPreferredPlatform (state, value) {
  state.userPreferredPlatform = value
}

export function updateIsLeftDrawerOpened (state, value) {
  state.isLeftDrawerOpened = value
}

export function reset (state) {
  state = initialState()
}

export function setLastPushNotificationOpenedAt (state, value) {
  state.lastPushNotificationOpenedAt = value
}

export function setLastNetworkErrorShownAt (state, value) {
  state.lastNetworkErrorShownAt = value
}

export function setTermsAndConditionsModalOpen (state, value) {
  state.isTermsAndConditionsModalOpen = value
}

export function setLastUploadedPhotoAt (state, value) {
  state.lastUploadedPhotoAt = value
}

export function setLastPhotoUploadFailureAt (state, value) {
  state.lastPhotoUploadFailureAt = value
}

export function setLastNotificationsPermissionWarningShownAt (state, value) {
  state.lastNotificationsPermissionWarningShownAt = value
}

export function setDisplayPKAsOxyde (state, value) {
  state.displayPKAsOxyde = value
}

export function setIncludePlannedFertilization (state, value) {
  state.includePlannedFertilization = value
}

export function setShowDetailedNutrientBalance (state, value) {
  state.showDetailedNutrientBalance = value
}
