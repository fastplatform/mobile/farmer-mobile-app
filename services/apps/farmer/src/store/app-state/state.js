import { platformToDisplay } from 'src/utils/device'

export const initialState = () => {
  return {
    currentPlatform: platformToDisplay(null),
    userPreferredPlatform: null,
    // open the left drawer on desktop version by default
    isLeftDrawerOpened: platformToDisplay(null) === 'desktop',
    // most recent timestamp when the app was opened via a push notification
    lastPushNotificationOpenedAt: null,
    // most recent timestamp when network error popup was shown
    lastNetworkErrorShownAt: null,
    // flag indicating if is the 'Terms & Conditions' modal is open
    isTermsAndConditionsModalOpen: false,
    // most recent timestamp when photo successfully uploaded
    lastUploadedPhotoAt: null,
    // most recent timestamp when photo upload failed
    lastPhotoUploadFailureAt: null,
    // most recent timestamp when notification permission warning popup was shown
    lastNotificationsPermissionWarningShownAt: null,
    // fertilization preferences
    displayPKAsOxyde: false,
    includePlannedFertilization: false,
    showDetailedNutrientBalance: false
  }
}

export default function () {
  return initialState()
}
