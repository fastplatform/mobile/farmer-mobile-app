import { InMemoryCache, defaultDataIdFromObject } from '@apollo/client/core'
import { CachePersistor } from 'apollo3-cache-persist'
import localforage from 'localforage'

// Ids for data denormalization
function dataIdFromObject (responseObject) {
  if ('id' in responseObject) {
    // standard nodes => we use the default id
    return defaultDataIdFromObject(responseObject)
  } else {
    // enums and enum items => we use the name
    return responseObject.__typename + '.' + responseObject.name
  }
}

// Instantiate the cache
const cache = new InMemoryCache({
  addTypename: true,
  dataIdFromObject,
  cacheRedirects: {
    Query: {
      plot_by_pk: (_, args) => {
        return dataIdFromObject({ __typename: 'plot', id: args.id })
      }
    }
  }
})

// The in-cache persistence to local storage
const persistor = new CachePersistor({
  cache,
  storage: localforage,
  debug: process.env.DEV
})

export { cache, persistor }
