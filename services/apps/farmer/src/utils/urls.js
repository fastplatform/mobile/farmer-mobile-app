import store from 'src/store'
import axios from 'axios'

export function getMediaUrl (mediaRelativeUrl) {
  // get the base media url
  let url = store.state.fastplatform.config.media
  // 'https://localhost.fastplatform.eu:49000/media'
  if (!url.endsWith('/')) {
    url += '/'
  }
  // add the media relative url
  if (mediaRelativeUrl.startsWith('/')) {
    // remove the leading '/'
    url += mediaRelativeUrl.substr(1, 1)
  } else {
    url += mediaRelativeUrl
  }
  return url
}

export async function getMediaSrc (mediaRelativeUrl) {
  const url = getMediaUrl(mediaRelativeUrl)

  return await axios.get(
    url,
    {
      responseType: 'blob',
      headers: {
        Authorization: 'Bearer ' + store.state.fastplatform.tokens.accessToken
      }
    }
  ).then(response => response.data)
    .then(blob => URL.createObjectURL(blob))
}

export function getRegionImageSrc (regionConfigImage) {
  //! temp hack to support old pathfinder config
  const imagesToken = 'images/'
  const imagesTokenIndex = regionConfigImage.lastIndexOf(imagesToken)
  const imageNameIndex = imagesTokenIndex < 0 ? 0 : imagesTokenIndex + imagesToken.length
  const imageName = regionConfigImage.slice(imageNameIndex)
  return '/images/regions/' + imageName
}
