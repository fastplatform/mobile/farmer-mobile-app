import { setQuasarLanguage } from 'src/services/translate'

export const LanguageMixin = {
  methods: {
    async setPlatformLanguage (languageCode) {
      // Set the language for vue-gettext
      this.$language.current = languageCode
      // Set the language for the Quasar components
      await setQuasarLanguage(languageCode)
      // Save the preferred language in the local storage
      this.$store.commit('fastplatform/updatePreferredLanguage', languageCode)
    }
  }
}
