import { getI18nField } from 'src/services/translate'

export const I18nFieldMixin = {
  methods: {
    displayI18nField (object, fieldName) {
      return getI18nField(object, fieldName)
    }
  }
}
