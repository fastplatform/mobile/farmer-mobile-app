export const ConfirmMixin = {
  methods: {
    confirm (dialogOptions) {
      return new Promise((resolve, reject) => {
        this.$q.dialog(
          dialogOptions
        ).onOk(() => {
          resolve(true)
        }).onCancel(() => {
          resolve(false)
        })
      })
    }
  }
}
