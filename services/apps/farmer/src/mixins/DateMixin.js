export const DateMixin = {
  methods: {
    displayDate (dateTime) {
      if (dateTime == null) {
        return ''
      }
      try {
        const utcDate = new Date(dateTime)
        const options = {
          month: 'short',
          day: 'numeric'
        }
        if (utcDate.getYear() !== new Date().getYear()) {
          options.year = 'numeric'
        }
        return new Intl.DateTimeFormat(this.$store.state.fastplatform.preferredLanguage, options).format(utcDate)
      } catch (e) {
        return ''
      }
    },

    displayDateTime (dateTime) {
      if (dateTime == null) {
        return ''
      }
      try {
        const utcDate = new Date(dateTime)
        return new Intl.DateTimeFormat(this.$store.state.fastplatform.preferredLanguage, {
          year: 'numeric',
          month: 'short',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric'
        }).format(utcDate)
      } catch (e) {
        console.error(e)
        return ''
      }
    },

    /** Display hour if dateTime is today else display date  */
    displayHourIfTodayElseDate (dateTime) {
      if (dateTime == null) {
        return ''
      }
      try {
        const utcDate = new Date(dateTime)

        // Compute the same day but at 00:00:00 in the morning *in local time* of the browser
        const timeZoneOffset = new Date().getTimezoneOffset()
        const todayAtMidnight = new Date()
        todayAtMidnight.setHours(0)
        todayAtMidnight.setMinutes(-timeZoneOffset)
        todayAtMidnight.setSeconds(0)

        // Timezone name
        const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone

        const currentLanguage = this.$store.state.fastplatform.preferredLanguage

        if (utcDate < todayAtMidnight) {
          if (utcDate.getFullYear() < todayAtMidnight.getFullYear()) {
            return new Intl.DateTimeFormat(currentLanguage, {
              year: 'numeric',
              month: 'short',
              day: 'numeric',
              timeZone
            }).format(utcDate)
          } else {
            return new Intl.DateTimeFormat(currentLanguage, {
              month: 'short',
              day: 'numeric',
              timeZone
            }).format(utcDate)
          }
        }

        return new Intl.DateTimeFormat(currentLanguage, {
          hour: 'numeric',
          minute: 'numeric',
          timeZone
        }).format(utcDate)
      } catch (e) {
        console.error(e)
        return ''
      }
    },

    displayFromNow (dateTime) {
      if (dateTime == null) {
        return ''
      }
      try {
        const seconds = Math.max(Math.floor((new Date().getTime() - new Date(dateTime).getTime()) / 1000), 0)
        if (seconds < 10) {
          return this.$gettext('Just now')
        }
        if (seconds < 60) {
          return seconds === 1 ? this.$gettext('1 second ago') : this.$gettextInterpolate(this.$gettext('%{seconds} seconds ago'), { seconds })
        }
        const minutes = Math.floor(seconds / 60)
        if (minutes < 60) {
          return minutes === 1 ? this.$gettext('1 minute ago') : this.$gettextInterpolate(this.$gettext('%{minutes} minutes ago'), { minutes })
        }
        const hours = Math.floor(minutes / 60)
        if (hours < 24) {
          return hours === 1 ? this.$gettext('1 hour ago') : this.$gettextInterpolate(this.$gettext('%{hours} hours ago'), { hours })
        }
        const days = Math.floor(hours / 24)
        if (days <= 31) {
          return days === 1 ? this.$gettext('1 day ago') : this.$gettextInterpolate(this.$gettext('%{days} days ago'), { days })
        }
        const months = Math.floor(days / 30.5)
        if (months < 12) {
          return months === 1 ? this.$gettext('1 month ago') : this.$gettextInterpolate(this.$gettext('%{months} months ago'), { months })
        }
        const years = Math.floor(months / 12)
        return years === 1 ? this.$gettext('1 year ago') : this.$gettextInterpolate(this.$gettext('%{years} years ago'), { years })
      } catch (e) {
        console.error(e)
        return ''
      }
    }
  }
}
