// Add a computed property to get the current platform. Used to adapt the pages / components layout.

export const isMobileApp = (platform) => Boolean(platform.is.mobile && platform.is.capacitor)
export const isIos = (platform) => Boolean(platform.is.mobile && platform.is.capacitor && platform.is.ios)
export const isAndroid = (platform) => Boolean(platform.is.mobile && platform.is.capacitor && platform.is.android)

export const PlatformMixin = {
  computed: {
    isDesktopPlatform () {
      // use the store in dev mode to dynamically change the displayed layout
      return process.env.DEV ? this.$store.state.appState.currentPlatform === 'desktop' : this.$q.platform.is.desktop
    },
    isMobileApp () {
      return Boolean(this.$q.platform.is.mobile && this.$q.platform.is.capacitor)
    },
    isIos () {
      return Boolean(this.$q.platform.is.mobile && this.$q.platform.is.capacitor && this.$q.platform.is.ios)
    },
    isAndroid () {
      return Boolean(this.$q.platform.is.mobile && this.$q.platform.is.capacitor && this.$q.platform.is.android)
    }
  }
}
