import { scroll } from 'quasar'

const { getScrollTarget, setVerticalScrollPosition } = scroll

export const ScrollMixin = {

  methods: {
    scrollToElement (el, duration = 1000) {
      const target = getScrollTarget(el)
      const offset = el.offsetTop
      setVerticalScrollPosition(target, offset, duration)
    }
  }

}
