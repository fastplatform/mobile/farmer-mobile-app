
// Method(s) for router action
export const RouterMixin = {
  methods: {
    goBack (route = null) {
      if (typeof route === 'string') {
        this.$router.replace(route)
      } else if (route && route.name) {
        this.$router.replace(route)
      } else {
        // Default behavior is to go back in history if possible, redirect to the home page if not
        window.history.length > 1 ? this.$router.go(-1) : this.$router.push({ name: 'home' })
      }
    }
  }
}
