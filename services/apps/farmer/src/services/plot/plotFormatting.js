import { getI18nField } from 'src/services/translate'

/** Format given plot area in m2 map by unit
 * !old formatting function: use display-surface-unit.formatSurface
*/
export const formatPlotAreaByUnit = area => {
  const plotAreaInHectare = (area / 10000)
  return {
    m2: Number(area).toFixed(1),
    ha: plotAreaInHectare.toFixed(1),
    unitToUse: plotAreaInHectare < 0.1 ? 'm2' : 'ha'
  }
}

/** Check if given plantVariety name is valid */
export const isPlotPlantVarietyNameValid = plantVariety => {
  return plantVariety?.name?.length > 1
}

/** Format given plantVariety */
export const formatPlotPlantVariety = plantVariety => {
  if (isPlotPlantVarietyNameValid(plantVariety)) {
    return getI18nField(plantVariety, 'name')
  }
  return ''
}

/** Format given plantVariety with fallback based on plantSpecies name */
export const formatPlotPlantVarietyWithFallback = (plantVariety, plantSpecies) => {
  return formatPlotPlantVariety(plantVariety) || `${getI18nField(plantSpecies, 'name') ?? plantVariety?.id ?? '???'}`
}

/** Sort given plotPlantVarieties according to percent usage and name */
export const getSortedPlotPlantVarieties = plotPlantVarieties => {
  if (!plotPlantVarieties) {
    return []
  }
  return plotPlantVarieties.slice().sort((a, b) => {
    if (a.percentage === null || a.percentage === undefined) {
      return -1
    } else if (b.percentage === null || b.percentage === undefined) {
      return 1
    } else if (a.percentage === b.percentage) {
      return a.id - b.id
    } else {
      return b.percentage - a.percentage
    }
  })
}

/** Compute available area percentage based on existing plotPlantVarieties */
export const getPlotAvailableAreaPercentage = (plotPlantVarieties, excludedPlotPlantVarietyId) => {
  if (plotPlantVarieties?.length) {
    return plotPlantVarieties
      .filter(plotPlantVariety => plotPlantVariety.id !== excludedPlotPlantVarietyId)
      .reduce((prev, curr) => prev - (curr.percentage ?? 0), 100)
  } else {
    return 100
  }
}
