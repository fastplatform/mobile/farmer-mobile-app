export function isUserMediaAvailable () {
  return 'mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices
}
