import { Notify } from 'quasar'
import { v4 as uuidv4 } from 'uuid'
import { Directory, Encoding } from '@capacitor/filesystem'
import { Network } from '@capacitor/network'

import { apolloClient } from 'src/boot/apollo'
import store from 'src/store'
import { $gettext } from 'src/boot/translate'
import { fileRead, fileWrite, fileDelete, readdir, mkdir, rmdir } from 'src/utils/file-system'
import MUTATION_UPLOAD_PHOTO_BASE64 from 'src/services/photo/graphql/mutationUploadGeoTaggedPhotoBase64'

const UPLOAD_MAX_RETRY = 3

const UPLOAD_ERRORS = {
  NO_NETWORK: 'No network connection available'
}

const FILESYSTEM_ERRORS = {
  FOLDER_DOES_NOT_EXIST: 'Folder does not exist',
  FILE_DOES_NOT_EXIST: 'File does not exist'
}

export const JOB_STATUS = Object.freeze({
  NOT_STARTED: 'not started', // not started or finished
  RUNNING: 'running', // is uploading files
  PENDING: 'pending' // running is paused while there is no network
})

export const FILE_UPLOAD_STATUS = Object.freeze({
  FAILED: 'failed',
  UPLOADED: 'uploaded',
  UPLOADING: 'uploading',
  PENDING: 'pending'
})

// The photo upload directory will be prefixed by the username. Ex.: toto/photos/to-upload or tata/photos/to-upload
const PHOTO_UPLOAD_DIR = 'photos/to-upload'

const SUPPORTED_IMAGE_EXTENSIONS = ['png', 'jpeg', 'jpg']

const MAP_FILE_EXTENSION_TO_MIME_TYPE = {
  png: 'image/png',
  jpg: 'image/jpeg',
  jpeg: 'image/jpeg'
}

/**
 * What does the upload service:
 *
 * 1) provides an 'upload' function that get a photo data and its metadata and store it to a folder inside the
 *    app data folder. It generates a uuid for the file name to avoid any name conflict.
 *    It also stores the metadata alongside the photo (the date time it was taken,
 *    the ID and the type of the object it is related to, etc.).
 *    It also starts the upload job, if not started.
 *
 * 2) An upload job that will upload any files that are in the upload directory. This job is started when a file is
 *    saved in the upload directory (using the 'upload' function) and stops when all the files inside this directory
 *    are uploaded or failed after a certain number of retries.
 *    The job will filter the files to get the image type files only and try to upload it alongside its metadata if
 *    there is a network connection available.
 *
 * Code errors:
 * - `1`:  Failed to read the "upload" directory. Therefore cannot upload photos.
 * - `2`:  Failed to parse metadata json file.
 * - `3`:  Failed to get the photo dimensions.
 * - `4`:  Failed to upload some photos.
 * - `5`:  Upload Request - GraphQL mutation - failed.
 * - `6`:  Failed to create upload directories
 * - `10`: Failed to read the photo png file.
 * - `11`: Failed to read the photo metadata file (json).
 * - `20`: Failed to delete the photo png file.
 * - `21`: Failed to delete the photo metadata file (json).
 * - `22`: Failed to delete the "username" directory (first directory of the upload path).
 * - `30`: Failed to write the photo png file.
 * - `31`: Failed to write the photo metadata file (json).
 */
export class UploadService {
  maxRetry = UPLOAD_MAX_RETRY
  isNetworkAvailable = null
  networkType = null
  networkWatchId = null
  jobStatus = JOB_STATUS.NOT_STARTED
  // Status about the uploaded files and the files that couldn't be uploaded
  // ex: fileStatus.uploaded = {fileNameA: {uploadedAt: ...}, fileNameB: {uploadedAt: ...}}
  // ex: fileStatus.failed = {fileNameC: {lastTriedAt: ...}, fileNameC: {lastTriedAt: ...}}
  // TODO add a way to delete or retry "failed" files
  filesStatus = {
    uploading: {},
    uploaded: {},
    failed: {}
  }

  username = null
  filesystemDirectory = null

  constructor (username, isIos) {
    this.username = username

    // return the file system directory depending on the platform. The directory used is the one dedicated to the application and therefore not "visible" to the user
    this.filesystemDirectory = isIos ? Directory.Documents : Directory.Data

    // create the upload dirs
    try {
      mkdir({
        path: this._getUploadDirectory(),
        directory: this.filesystemDirectory,
        recursive: true
      })
    } catch (e) {
      console.error(`[UPLOAD_PHOTOS_ERROR](Code 6) Failed to create upload directories ${this._getUploadDirectory()}:`, e)
    }
  }

  /**
   * Delete a photo (both the png and json files)
   * @param {String} file Name of the file to delete
   */
  async deleteFile (file) {
    // delete png file
    let filePath = this._getUploadDirectory() + '/' + file
    let isPhotoDataDeleted = false
    let isPhotoMetadataDeleted = false
    try {
      // the assertion is necessary to await the result. The result is an object containing information about the
      // deleted file. Here, we just want a boolean. So, if no error is raised, the file is deleted.
      isPhotoDataDeleted = await fileDelete(filePath)
      isPhotoDataDeleted = true
    } catch (e) {
      if (e.message && e.message.includes(FILESYSTEM_ERRORS.FILE_DOES_NOT_EXIST)) {
        // It's ok
        isPhotoDataDeleted = true
      } else {
        // TODO handle the case where the deletion failed
        console.error(`[UPLOAD_PHOTOS_ERROR](Code 20) Failed to delete file ${filePath}:`, e)
      }
    }
    // delete metadata file
    const fileInfo = this._getFileInfo(file)
    filePath = this._getUploadDirectory() + '/' + fileInfo.baseName + '.json'
    try {
      // the assertion is necessary to await the result. The result is an object containing information about the
      // deleted file. Here, we just want a boolean. So, if no error is raised, the file is deleted.
      isPhotoMetadataDeleted = await fileDelete(filePath)
      isPhotoMetadataDeleted = true
    } catch (e) {
      if (e.message && e.message.includes(FILESYSTEM_ERRORS.FILE_DOES_NOT_EXIST)) {
        // It's ok
        isPhotoMetadataDeleted = true
      } else {
        console.error(`[UPLOAD_PHOTOS_ERROR](Code 21) Failed to delete file ${filePath}:`, e)
      }
    }
    // remove the file from the "failed" list
    if (Object.keys(this.filesStatus.failed).includes(file)) {
      delete this.filesStatus.failed[file]
    }
    return isPhotoDataDeleted && isPhotoMetadataDeleted
  }

  /**
   * Delete all the photos even if the uploading process is started and delete the upload directory.
   * This function is called when a user logs out and all his / her data need to be erased.
   */
  async deletePhotosAndDirectory () {
    try {
      const res = await rmdir({
        // delete all the "username" folder, currently it contains only the photos but may contains other things later
        path: this.username,
        directory: this.filesystemDirectory,
        recursive: true
      })
      if (res?.error) {
        console.error(`[UPLOAD_PHOTOS_ERROR](Code 22) Failed to delete directory ${this.username + '/'}:`, res.error)
        return false
      } else {
        return true
      }
    } catch (e) {
      console.error(`[UPLOAD_PHOTOS_ERROR](Code 22) Failed to delete directory ${this.username + '/'}:`, e)
      return false
    }
  }

  /**
   * Check if the given file exists in the "upload" directory
   * @param {String} fileName Name of the file
   * @returns {Promise<{exists: null;error: any;} | {exists: boolean;error: null;}>}
   */
  async _doesFileExist (fileName) {
    const { files, error } = await this._getFilesToUpload()
    if (error) {
      return {
        exists: null,
        error
      }
    } else {
      return {
        exists: files.includes(fileName),
        error: null
      }
    }
  }

  /**
   * Read the content of a file
   * @param {String} filePath Path of the file to get the data from inside the `this.filesystemDirectory` directory
   * @param {FilesystemEncoding?} encoding The encoding (`FilesystemEncoding`) to use. Default to base64 if not specified
   */
  async _getFileData (filePath, encoding = null) {
    const options = {
      path: filePath,
      directory: this.filesystemDirectory
    }
    // the default encoding used is base64
    if (encoding) {
      options.encoding = encoding
    }
    try {
      const fileData = await fileRead(options)
      return fileData.data
    } catch (e) {
      console.error(`[UPLOAD_PHOTOS_ERROR](Code 10) Failed to read file ${filePath}:`, e)
      return null
    }
  }

  /**
   * Get the dimensions (width and height) of an image represented as a base64 encoded string
   * @param {String} fileDataBase64 Image data represented as a base64 encoded string
   * @returns {Promise<{width: Number, height: Number}>} The width and height of the image
   */
  _getFileDimensions (fileDataBase64) {
    return new Promise((resolve, reject) => {
      const img = new Image()
      const src = fileDataBase64.startsWith('data:image/png;base64,') ? fileDataBase64 : 'data:image/png;base64,' + fileDataBase64
      img.src = src
      img.onload = () => resolve({ width: img.width, height: img.height })
      img.onerror = err => reject(err)
    })
  }

  /**
   * Get info about the type of the file
   * @param {String} file Name of the file
   * @return {{name: String, baseName: String, extension: String, mimeType: String}} Basic info about the file
   */
  _getFileInfo (file) {
    const parts = file.split('.')
    const extension = parts[parts.length - 1]
    return {
      name: file,
      baseName: parts.slice(0, parts.length - 1).join('.'),
      extension,
      mimeType: MAP_FILE_EXTENSION_TO_MIME_TYPE[extension]
    }
  }

  /**
   * Read the metadata associated to a photo.
   * The metadata are stored in a json file under the same name as the photo file and are utf-8 encoded
   * @param {String} file Name of the photo image file (.png)
   * @returns {Promise<JSON>} The metadata as a JSON object or null if it failed
   */
  async _getFileMetadata (file) {
    const fileInfo = this._getFileInfo(file)
    const filePath = this._getUploadDirectory() + '/' + fileInfo.baseName + '.json'
    try {
      const rawMetadata = await this._getFileData(filePath, Encoding.UTF8)
      try {
        return JSON.parse(rawMetadata)
      } catch (e) {
        console.error('[UPLOAD_PHOTOS_ERROR](Code 2) Failed to parse metadata for the file', file)
        return null
      }
    } catch (e) {
      console.error(`[UPLOAD_PHOTOS_ERROR](Code 11) Failed to read file ${filePath}:`, e)
    }
  }

  _getFileObject (fileData, fileInfo) {
    const fo = new Blob(fileData, { type: fileInfo.mimeType })
    fo.name = fileInfo.name
    return fo
    // return new File(fileData, fileInfo.name, { type: fileInfo.mimeType })
  }

  /**
   * Returns all the image files (name) of the "upload" directory
   * @returns {Promise<{files: String[];error: null;} | {error: any;files: null;}>}
   */
  async _getFilesToUpload () {
    // return all the png files to upload
    const filesRes = await readdir({
      path: this._getUploadDirectory(),
      directory: this.filesystemDirectory
    })
    if (!filesRes.error) {
      // filter all non image files
      const files = filesRes.files.filter(f => {
        for (let i = 0; i < SUPPORTED_IMAGE_EXTENSIONS.length; i++) {
          if (f.endsWith(SUPPORTED_IMAGE_EXTENSIONS[i])) {
            return true
          }
        }
        return false
      })
      return {
        files
      }
    } else {
      if (filesRes.error.message.includes(FILESYSTEM_ERRORS.FOLDER_DOES_NOT_EXIST)) {
        // the upload folder does not exist yet. Therefore, no photos to upload
        return {
          error: null,
          files: []
        }
      } else {
        return {
          error: filesRes.error
        }
      }
    }
  }

  _getFileUploadStatus (fileName) {
    if (Object.keys(this.filesStatus.failed).includes(fileName)) {
      return FILE_UPLOAD_STATUS.FAILED
    } else if (Object.keys(this.filesStatus.uploaded).includes(fileName)) {
      return FILE_UPLOAD_STATUS.UPLOADED
    } else if (Object.keys(this.filesStatus.uploading).includes(fileName)) {
      return FILE_UPLOAD_STATUS.UPLOADING
    } else {
      return FILE_UPLOAD_STATUS.PENDING
    }
  }

  /**
   * Get the photo data and metadata. Return the photo in a photoObject:
   * ```
   * photoObject: {
   *  photo: photoImgData, // base64 encoded string
   *  ...metadata // all the metadata at the root of the object
   * }
   * ```
   * @type {photoObject}
   * @param {String} fileName Name of the file
   * @returns {Promise<{photoObject: any, error: null} | {photoObject: null, error: any}}>}
   */
  async getPhoto (fileName) {
    // Check if the file exists
    const { exists, error } = await this._doesFileExist(fileName)
    if (exists) {
      const metadata = await this._getFileMetadata(fileName)
      const filePath = this._getUploadDirectory() + '/' + fileName
      const fileData = await this._getFileData(filePath)
      return {
        photoObject: {
          photo: fileData,
          ...metadata,
          file_name: fileName,
          local: true,
          status: this._getFileUploadStatus(fileName)
        },
        error: null
      }
    } else {
      if (error) {
        return {
          photoObject: null,
          error
        }
      } else {
        return {
          photoObject: null,
          error: 'NotFound'
        }
      }
    }
  }

  /**
   * Get the network status and set the result in local variables
   */
  async _getNetworkStatus () {
    try {
      const status = await Network.getStatus()
      this.isNetworkAvailable = status.connected
      this.networkType = status.connectionType
    } catch (_) {
      this.isNetworkAvailable = null
      this.networkType = null
    }
  }

  /**
   * Return the path to the directory that contains the photo to upload.
   * It is defined as follow: <username>/<PHOTO_UPLOAD_DIR>, where username is the username of the current logged in
   * user and PHOTO_UPLOAD_DIR is a constant value that defines a path to use for the upload directory.
   */
  _getUploadDirectory () {
    return this.username + '/' + PHOTO_UPLOAD_DIR
  }

  /**
   * List of the local photos that are yet to be uploaded.
   * The `photos` list contains `photo` objects represented as follows:
   * ```
   * {
   *  photo: string, // photo data (base64 encoded string)
   *  ...metadata, // all the metadata directly at the root of the object
   *  file_name: string, // the name of the image file (.png)
   *  local: true, // a flag to indicate that the photo is local
   *  status: string // the status of the file (pending, failed, uploading)
   * }
   * ```
   * @param {Object} filters Optional filters ({ holdingCampaignId, plotId })
   * @returns {Promise<{photos: Array?, error?: any, message?: String, photoErrors?: Array?}>} Local photos
   */
  async listPhotosToUpload (filters) {
    // list the photos to upload for display purpose
    // get the files
    const res = await this._getFilesToUpload()
    if (res.error) {
      return {
        photos: null,
        error: res.error,
        message: 'Failed to get the local photos'
      }
    }
    return new Promise((resolve, reject) => {
      const files = res.files
      const photos = []
      const errors = []
      const promises = []
      // for each file, get the associated metadata and raw data (image data)
      files.forEach(async (f) => {
        const filePath = this._getUploadDirectory() + '/' + f
        promises.push(
          Promise.all([this._getFileData(filePath), this._getFileMetadata(f)])
            .then(([fileData, metadata]) => {
              let listPhoto = true

              if (filters) {
                // filter by holding campaign if applicable
                if (filters.holdingCampaignId && metadata.holding_campaign_id !== filters.holdingCampaignId) {
                  listPhoto = false
                }
                // filter by plot if applicable
                if (filters.plotId && metadata.plot_id !== filters.plotId) {
                  listPhoto = false
                }
              }

              if (listPhoto) {
                photos.push({
                  photo: fileData,
                  ...metadata,
                  file_name: f,
                  local: true,
                  status: this._getFileUploadStatus(f)
                })
              }
            })
            .catch(error => {
              errors
                .push({
                  fileName: f,
                  error,
                  message: 'Failed to read the info for this file'
                })
                .finally(() => Promise.resolve())
            })
        )
      })
      Promise.all(promises).finally(() => resolve({
        photos,
        photoErrors: errors
      }))
    })
  }

  /**
   * Shortcut to get local photos for a holding campaign.
   */
  async listPhotosToUploadByHoldingCampaign (holdingCampaignId) {
    return this.listPhotosToUpload({ holdingCampaignId })
  }

  /**
   * Shortcut to get local photos for a plot.
   */
  async listPhotosToUploadByPlot (plotId) {
    return this.listPhotosToUpload({ plotId })
  }

  /**
   * Save a photo locally and start the upload process if not already started
   * The photo is stored in two files with the same name (uuid generated name):
   * - a .png file that contains the image data
   * - a .json file that contains the metadata
   * @param {String} photoData Base64 encoded string that contains the photo image data
   * @param {any} photoMetadata A object containing the metadata of the photo
   */
  async savePhoto (photoData, photoMetadata) {
    // TODO Check that the photoMetadata contains at least one of the following id: plot, site, holding_campaign
    const fileName = uuidv4()
    // save the photo data - base64 encoded
    let res = await fileWrite({
      data: photoData,
      path: this._getUploadDirectory() + '/' + fileName + '.png',
      directory: this.filesystemDirectory,
      recursive: true
    })
    if (res.error) {
      console.error(`[UPLOAD_PHOTOS_ERROR](Code 30) Failed to write file ${this._getUploadDirectory() + '/' + fileName + '.png'}:`, res.error)
      return {
        error: res.error,
        message: $gettext('Failed to save the photo. Cannot write photo file.')
      }
    }
    // Add the photo dimensions to the metadata
    try {
      photoMetadata.dimensions = await this._getFileDimensions(photoData)
    } catch (e) {
      console.error(`[UPLOAD_PHOTOS_ERROR](Code 3) Failed to get file ${this._getUploadDirectory() + '/' + fileName + '.json'} dimensions:`, res.error)
    }
    photoMetadata = JSON.stringify(photoMetadata)
    // save the metadata using the same file name - utf-8 encoded
    res = await fileWrite({
      data: photoMetadata,
      path: this._getUploadDirectory() + '/' + fileName + '.json',
      encoding: Encoding.UTF8,
      directory: this.filesystemDirectory,
      recursive: false // already done
    })
    if (res.error) {
      console.error(`[UPLOAD_PHOTOS_ERROR](Code 31) Failed to write file ${this._getUploadDirectory() + '/' + fileName + '.json'}:`, res.error)
      return {
        error: res.error,
        message: 'Failed to save the photo metadata. Cannot write photo metadata file.'
      }
    }
    // start the upload as an asynchronous task
    this.startUpload({ notifyOnError: true })

    // indicate that the photo has been saved as a returned value
    return {
      success: true
    }
  }

  /**
   * Upload photo directly without saving it on the storage first.
  */
  async uploadPhoto (photoData, photoMetadata) {
    const fileName = uuidv4() + '.png'
    try {
      photoMetadata.dimensions = await this._getFileDimensions(photoData)
    } catch (e) {
      console.error('Failed to get the web photo dimensions', e)
    }
    const { success, error } = await this._uploadRequestBase64({ data: photoData, name: fileName }, photoMetadata)
    if (!success) {
      console.error('Could not upload photo', error)
      throw new Error('Could not upload photo', { cause: error })
    }
  }

  /**
   * Start the upload job. Start a network watcher to detect the changes in the device network
   */
  async startUpload (options) {
    if (this.jobStatus === JOB_STATUS.NOT_STARTED) {
      // Get the status of the network and react to any change while the file(s) are not uploaded
      await this._getNetworkStatus() // wait the network status before starting the upload
      await this._watchNetwork()
    }
    if (options?.retryFailedFiles) {
      // remove the failed flags from all files
      this.filesStatus.failed = {}
    }
    // If the status is not NOT_STARTED, then the network is already "watched"
    this._uploadFiles(options?.notifyOnError)
  }

  /**
   * Stop the network watcher
   */
  _stopWatchingNetwork () {
    if (this.networkWatchId) {
      this.networkWatchId.remove()
      this.networkWatchId = null
    }
  }

  /**
   * Upload a file. Try to upload it a certain number of times as specified in the class variable `maxRetry`.
   * If the file could not be uploaded after all the retries, flag it as failed.
   * If the network is not available, stop the upload process and return a network error flag. This does not flag
   * the file as failed.
   * @param {String} file Name of the file
   */
  async _uploadFile (file) {
    // set the currentlyUploading flag
    this.filesStatus.uploading[file] = { startedAt: Date.now() }
    let isUploaded = false
    let retry = this.maxRetry
    let uploadError = null
    let metadata = null
    let fileData = null
    try {
      // Get the metadata
      metadata = await this._getFileMetadata(file)
      const filePath = this._getUploadDirectory() + '/' + file
      fileData = await this._getFileData(filePath)
    } catch (e) {
      // remove the file from the uploading group
      delete this.filesStatus.uploading[file]
      return { success: false, error: e, context: null }
    }
    // Retry if the upload fails
    while (!isUploaded && retry > 0) {
      // If the network is not available, stop the upload and set the job status to PENDING
      if (!this.isNetworkAvailable) {
        // remove the file from the uploading group
        delete this.filesStatus.uploading[file]
        return { success: false, error: UPLOAD_ERRORS.NO_NETWORK, context: null }
      }
      // try to upload the file
      const { success, error } = await this._uploadRequestBase64({ data: fileData, name: file }, metadata)
      isUploaded = success
      uploadError = error
      // Decrease the retry counter if the upload failed
      if (!isUploaded) {
        // wait 3s before next try
        retry = await new Promise((resolve) => setTimeout(() => resolve(retry - 1), 3000))
      }
    }
    // remove the file from the uploading group
    delete this.filesStatus.uploading[file]
    return {
      success: isUploaded,
      error: uploadError,
      context: {
        holding_campaign_id: metadata.holding_campaign_id || null,
        holding_campaign_holding_name: metadata.holding_campaign_holding_name || null,
        holding_campaign_campaign_name: metadata.holding_campaign_campaign_name || null
      }
    }
  }

  /**
   * Upload all the photos that are listed in the "upload" directory
   * Pause the upload process if the network connection is turned off and resume it as soon as the network connection
   * is available.
   */
  async _uploadFiles (notifyOnError) {
    function handleReadDirErrorDisplayMessage () {
      Notify.create({
        // TODO better message
        message: $gettext('Cannot upload your photos. Cannot read the directory that contains the photos.'),
        color: 'negative'
      })
      console.error('[UPLOAD_PHOTOS_ERROR](Code 1) Cannot get the files to upload.', res.error)
    }
    this.jobStatus = JOB_STATUS.RUNNING
    const res = await this._getFilesToUpload()
    if (res.error) {
      handleReadDirErrorDisplayMessage()
      // Remove the "watcher" and change the job status
      this._stopWatchingNetwork()
      this.jobStatus = JOB_STATUS.NOT_STARTED
      return
    }
    // Do not filter out the files marked as "failed" to give them another try
    let files = res.files
    let file

    // Upload all listed files. At the end of the process, check if new files to upload were added during the process
    // In that case, upload them and check again. If not, end the upload process.
    while (files.length > 0) {
      // upload all the listed files
      while (files.length > 0) {
        // get the first file of the list
        file = files[0]
        // check if the file has already been uploaded
        if (Object.keys(this.filesStatus.uploaded).includes(file)) {
          // delete it from the disk
          await this.deleteFile(file)
        } else if (!Object.keys(this.filesStatus.uploading).includes(file)) {
          // upload the file
          const { success, error, context } = await this._uploadFile(file)
          if (!success) {
            // check if there is a network connection issue
            if (error === UPLOAD_ERRORS.NO_NETWORK) {
              // There is no network available for the moment. Stop the upload process and mark it as 'pending'
              // The upload will start again when the network will be back
              console.warn('NO NETWORK - SET JOB AS PENDING')
              this.jobStatus = JOB_STATUS.PENDING
              return
            } else {
              // the file couldn't be uploaded, add it to the "failed" list
              this.filesStatus.failed[file] = { lastTriedAt: Date.now(), error, context }
            }
            // add the last upload failure at to the store (so that components may listen to this event)
            store.commit('appState/setLastPhotoUploadFailureAt', Date.now())
          } else {
            // add the file to the "uploaded" list
            this.filesStatus.uploaded[file] = { uploadedAt: Date.now() }
            // Delete the file (and its metadata)
            await this.deleteFile(file)
            // add the last uploaded at to the store (so that components may listen to this event)
            store.commit('appState/setLastUploadedPhotoAt', Date.now())

            console.log('File ' + file + ' successfully uploaded at ' + new Date())
          }
        }
        // remove the file from the list (remove first element)
        files.splice(0, 1)
      }
      // Check if new files were added during the upload process
      const res = await this._getFilesToUpload()
      if (res.error) {
        handleReadDirErrorDisplayMessage()
        // Remove the "watcher" and change the job status
        this._stopWatchingNetwork()
        this.jobStatus = JOB_STATUS.NOT_STARTED
        // stop looping
        return
      } else {
        files = res.files
        // filter the files marked as "failed"
        files = files.filter(f => !Object.keys(this.filesStatus.failed).includes(f))
      }
    }
    // file upload is over. Remove the "watcher" and change the job status
    this._stopWatchingNetwork()
    this.jobStatus = JOB_STATUS.NOT_STARTED
    if (Object.keys(this.filesStatus.failed).length > 0) {
      if (notifyOnError) {
        Notify.create({
          message: $gettext('The photo upload failed: it will be automatically retried.'),
          color: 'negative',
          timeout: 10000 // 10 seconds
        })
      }
      console.error(`[UPLOAD_PHOTOS_ERROR](Code 4) Failed to upload some files: ${Object.keys(this.filesStatus.failed)}`)
    }
  }

  /**
   * Send a graphql mutation to upload the photo
   * @param {{data: String, name: String}} imageData `data` contains the image data as base64 encoded string. `name` is the name of the file.
   * @param {any} metadata The photo metadata
   * @returns {Promise<{success: Boolean, error: String}>} Status of the upload
   */
  _uploadRequestBase64 ({ data, name }, metadata) {
    // apollo mutation
    return new Promise((resolve) => {
      apolloClient.mutate({
        mutation: MUTATION_UPLOAD_PHOTO_BASE64,
        variables: {
          heading: metadata.heading,
          holding_campaign_id: Number(metadata.holding_campaign_id),
          photo_data_base64: data,
          photo_name: name,
          photo_width: Number(metadata.dimensions.width),
          photo_height: Number(metadata.dimensions.height),
          shot_at: metadata.shot_at,
          location: JSON.stringify(metadata.location),
          description: metadata.description,
          elevation: metadata.elevation != null ? Number(metadata.elevation) : null,
          horizontal_dilution_of_precision: metadata.horizontal_dilution_of_precision != null ? Number(metadata.horizontal_dilution_of_precision) : null,
          plot_id: metadata.plot_id != null ? Number(metadata.plot_id) : null,
          vertical_dilution_of_precision: metadata.vertical_dilution_of_precision != null ? Number(metadata.vertical_dilution_of_precision) : null
        }
      }).then((res) => {
        resolve({ success: res.data.upload_geo_tagged_photo_base64.success, error: null })
      }).catch((err) => {
        console.error(`[UPLOAD_PHOTOS_ERROR](Code 5) Upload request for file ${name} failed:`, err)
        resolve({ success: false, error: err })
      })
    })
  }

  /**
   * Watch the network changes. If the network becomes available and the upload job was paused, then resume it.
   */
  async _watchNetwork () {
    this.networkWatchId = await Network.addListener('networkStatusChange', (status) => {
      this.isNetworkAvailable = status.connected
      this.networkType = status.connectionType
      // if the job status is "pending" (waiting for network) and the network is available, then restart the upload job
      if (this.jobStatus === JOB_STATUS.PENDING && this.isNetworkAvailable) {
        this.startUpload()
      }
    })
  }

  /**
   * Check that there is no running or pending job nor any failed file.
   */
  isUploadJobComplete () {
    return this.jobStatus === JOB_STATUS.NOT_STARTED && Object.keys(this.filesStatus.uploading).length === 0 && Object.keys(this.filesStatus.failed).length === 0
  }
}

/** The service instance accessd by the components */
export let uploadServiceInstance = null

export function initializeUploadService (isIos) {
  const user = store.state.fastplatform.user
  uploadServiceInstance = new UploadService(user.username, isIos)
}

export function stopUploadService () {
  uploadServiceInstance = null
}
