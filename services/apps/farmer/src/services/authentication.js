import axios from 'axios'
import { OAuth2Client } from '@byteowls/capacitor-oauth2'

import store from 'src/store'

export const MOBILE_REDIRECT_URL = 'oauth.eu.fastplatform.mobile.farmer:/'

const authOptions = {
  appId: '',
  authorizationBaseUrl: '',
  accessTokenEndpoint: '',
  additionalParameters: {},
  android: {
    // DO NOT REMOVE pkceEnabled. Overwriting the pkceEnabled parameter in the android specific options is required to
    // avoid an issue with auth request. If not specified here, it seems a "default" value set to false is used instead,
    // overwriting the one set in the global options.
    pkceEnabled: true,
    redirectUrl: MOBILE_REDIRECT_URL
  },
  ios: {
    pkceEnabled: true,
    redirectUrl: MOBILE_REDIRECT_URL
  },
  pkceEnabled: true,
  responseType: 'code',
  scope: 'openid offline_access profile email',
  state: undefined,
  web: {
    redirectUrl: window.location.origin + '/login/cb',
    windowOptions: null // let options empty to open in tab instead of window
  }
}

export function login (customOptions) {
  const options = { ...authOptions, ...customOptions }
  return new Promise((resolve, reject) => {
    OAuth2Client.authenticate(options).then(resourceUrlResponse => {
      //! the tokens may be located within the access_token_response field or the main object depeding on the implementation
      const accessTokenResponse = resourceUrlResponse.access_token_response || resourceUrlResponse
      const accessToken = accessTokenResponse.access_token
      const oauthUserId = accessTokenResponse.id_token
      const tokenType = accessTokenResponse.token_type
      const refreshToken = accessTokenResponse.refresh_token

      const tokens = {
        accessToken,
        idToken: oauthUserId,
        tokenType,
        refreshToken
      }
      resolve(tokens)
      // go to backend
    }).catch((reason) => {
      console.error('OAuth login failed', reason)
      reject(reason)
    })
  })
}

export function logout () {
  return new Promise((resolve, reject) => {
    OAuth2Client.logout(authOptions).then(() => {
      resolve()
      // do something
    }).catch((reason) => {
      console.error('OAuth logout failed', reason)
      reject(reason)
    })
  })
}

/**
 * Get a refreshed OAuth token.
*/
export function getRefreshedAuthToken () {
  console.log('User auth token expired: refreshing token...')
  if (!store.state?.fastplatform || !store.getters['fastplatform/isRefreshTokenConfigValid']) {
    console.warn('Cannot refresh token: invalid store config')
    return new Promise((resolve, reject) => {
      reject('Cannot refresh token: invalid store config')
    })
  }

  const loginConfig = store.state.fastplatform.config.login
  const clientId = loginConfig.clientId
  const refreshUrl = loginConfig.token_endpoint
  const refreshAuthToken = store.state.fastplatform.tokens.refreshToken

  const body = new URLSearchParams()
  body.append('client_id', clientId)
  body.append('grant_type', 'refresh_token')
  body.append('refresh_token', refreshAuthToken)

  const reqConfig = {
    headers: {
      'Content-type': 'application/x-www-form-urlencoded'
    }
  }

  return axios.post(refreshUrl, body, reqConfig).then(({ data }) => {
    const tokens = {
      accessToken: data.access_token,
      idToken: data.id_token,
      tokenType: data.token_type,
      refreshToken: data.refresh_token
    }
    store.commit('fastplatform/updateTokens', tokens)
    console.log('User auth token successfully refreshed')
    return tokens.accessToken
  }).catch(error => {
    if (error.response?.data?.error === 'invalid_grant') {
      throw new Error('Could not refresh OAuth token: invalid grant')
    } else {
      // do not logout user for other potential errors as it could be client side
      console.warn('Could not refresh OAuth token', error)
      return null
    }
  })
}
