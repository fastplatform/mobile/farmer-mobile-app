import { $gettext, $pgettext } from 'src/boot/translate'
import algorithms from 'src/services/fieldbook/fertilization/algorithms'

export const planTypes = Object.freeze({
  NORMAL: 'NORMAL',
  ROTATIONS: 'ROTATIONS'
})

export const chemicalElementsIds = Object.freeze({
  N: 'N',
  n: 'N',
  P: 'P',
  p: 'P',
  K: 'K',
  k: 'K',
  ca: 'CA',
  CA: 'CA',
  mg: 'MG',
  MG: 'MG'
})

export const npkChemicalElementIds = Object.freeze([
  chemicalElementsIds.N,
  chemicalElementsIds.P,
  chemicalElementsIds.K
])

export const chemicalFormulasIds = Object.freeze({
  P2O5: 'P2O5',
  K2O: 'K2O',
  p2o5: 'P2O5',
  k2o: 'K2O'
})

export const unitOfMeasures = Object.freeze({
  KILOGRAM_PER_HECTARE: 'kilogram_per_hectare',
  TON_PER_HECTARE: 'ton_per_hectare',
  QUINTAL_PER_HECTARE: 'quintal_per_hectare',
  KILOGRAM_PER_STREMMA: 'kilogram_per_stremma'
})

export const unitOfMeasuresSymbol = Object.freeze({
  kilogram_per_hectare: $pgettext('kilogram per hectare', 'kg/ha'),
  ton_per_hectare: $pgettext('ton per hectare', 't/ha'),
  quintal_per_hectare: $pgettext('quintal per hectare', 'q/ha'),
  kilogram_per_stremma: $pgettext('kilogram per stremma', 'kg/str')
})

export const resultTypes = Object.freeze({
  PLANT_NEEDS: 'PLANT_NEEDS',
  PLANT_SUPPLIES: 'PLANT_SUPPLIES',
  PLANNED: 'PLANNED',
  RECOMMENDATION: 'RECOMMENDATION',
  RECOMMENDATION_PER_TREE: 'RECOMMENDATION_PER_TREE',
  STANDARD_DOSE: 'STANDARD_DOSE',
  MAIN_PRODUCT_EXTRACTION: 'MAIN_PRODUCT_EXTRACTION',
  BY_PRODUCT_EXTRACTION: 'BY_PRODUCT_EXTRACTION',
  BIOLOGICAL_FIXATION: 'BIOLOGICAL_FIXATION',
  SOIL_MINERALIZATION: 'SOIL_MINERALIZATION',
  BALANCE: 'BALANCE',
  IN_SOIL_INITIAL: 'IN_SOIL_INITIAL',
  IRRIGATION: 'IRRIGATION',
  FERTILIZER_APPLIED: 'FERTILIZER_APPLIED',
  LEACHING: 'LEACHING',
  DENITRIFICATION: 'DENITRIFICATION',
  IN_SOIL_FINAL: 'IN_SOIL_FINAL',
  VOLATILIZATION: 'VOLATILIZATION',
  UPTAKE: 'UPTAKE',
  INPUTS: 'INPUTS',
  OUTPUTS: 'OUTPUTS'
})

export const resultTypesLabel = Object.freeze({
  PLANT_NEEDS: $gettext('plant needs'),
  PLANT_SUPPLIES: $gettext('plant supplies'),
  PLANNED: $gettext('fertilizer supply planned'),
  RECOMMENDATION: $gettext('recommendation'),
  STANDARD_DOSE: $gettext('standard dose'),
  MAIN_PRODUCT_EXTRACTION: $gettext('main product extraction'),
  BY_PRODUCT_EXTRACTION: $gettext('by-product extraction'),
  BIOLOGICAL_FIXATION: $gettext('biological fixation'),
  SOIL_MINERALIZATION: $gettext('recovered from soil'),
  BALANCE: $gettext('balance')
})

export const getNPKRecommendationsByPlot = (algorithmId, fertilizationPlanResults) => {
  const algorithm = algorithms[algorithmId]
  if (!algorithm?.hasNPKRecommendations) {
    return null
  } else if (algorithm.hasNewResultFormat) {
    // new format has only one plot per plan
    return {
      [fertilizationPlanResults.plot_id]: {
        recommendations: [fertilizationPlanResults].map(recommendation => ({
          ...recommendation,
          crop_yield_unit_of_measure_symbol: fertilizationPlanResults.crop_yield_unit || unitOfMeasuresSymbol[fertilizationPlanResults.crop_yield_unit_of_measure_id],
          element_recommendations: recommendation.element_recommendations.map(elementRecommendation => ({
            ...elementRecommendation,
            recommendation_type: elementRecommendation.result_type,
            quantity_unit_of_measure_symbol: unitOfMeasuresSymbol[elementRecommendation.unit_of_measure_id]
          }))
        }))
      }
    }
  }

  const npkRecommendations = fertilizationPlanResults.npk_recommendations ?? []
  if (npkRecommendations.length) {
    const npkRecommendationsByPlot = {}
    npkRecommendations.forEach(npkRecommendation => {
      if (npkRecommendationsByPlot[npkRecommendation.plot_id] == null) {
        npkRecommendationsByPlot[npkRecommendation.plot_id] = {
          plot_name: npkRecommendation.plot_name,
          recommendations: []
        }
      }
      npkRecommendationsByPlot[npkRecommendation.plot_id].recommendations.push({
        advices: npkRecommendation.advices,
        plot_id: npkRecommendation.plot_id,
        plot_name: npkRecommendation.plot_name,
        plant_species_id: npkRecommendation.plant_species_id,
        plant_species_name: npkRecommendation.plant_species_name,
        plant_species_profile_name: npkRecommendation.plant_species_profile_name,
        rotation_order: npkRecommendation.rotation_order,
        crop_yield: npkRecommendation.crop_yield,
        crop_yield_unit_of_measure_id: npkRecommendation.crop_yield_unit_of_measure_id,
        crop_yield_unit_of_measure_symbol: npkRecommendation.crop_yield_unit_of_measure_id
          ? unitOfMeasuresSymbol[npkRecommendation.crop_yield_unit_of_measure_id]
          : null,
        element_recommendations: npkRecommendation.element_recommendations != null
          ? npkRecommendation.element_recommendations
            .map(elementRecommendation => {
              return {
                chemical_element_id: elementRecommendation.chemical_element_id,
                chemical_formula_id: elementRecommendation.chemical_formula_id,
                quantity: elementRecommendation.quantity,
                quantity_min: elementRecommendation.quantity_min,
                quantity_max: elementRecommendation.quantity_max,
                quantity_unit_of_measure_id: elementRecommendation.unit_of_measure_id,
                quantity_unit_of_measure_symbol: elementRecommendation.unit_of_measure_id
                  ? unitOfMeasuresSymbol[elementRecommendation.unit_of_measure_id]
                  : null,
                recommendation_type: elementRecommendation.result_type
              }
            })
          : null
      })
      if (npkRecommendationsByPlot[npkRecommendation.plot_id].recommendations.length > 1) {
        // sort by order
        npkRecommendationsByPlot[npkRecommendation.plot_id].recommendations.sort((a, b) => {
          return a.rotation_order - b.rotation_order
        })
      }
    })
    return npkRecommendationsByPlot
  } else {
    return null
  }
}

export const DEFAULT_QUANTITY_UNIT = $gettext('kg')

export const DEFAULT_SURFACE_UNIT = $gettext('ha')

export const getDisplayUnit = displayUnitConfig => {
  const quantityUnit = displayUnitConfig?.quantity?.short_name || DEFAULT_QUANTITY_UNIT
  const surfaceUnit = displayUnitConfig?.surface?.short_name || DEFAULT_SURFACE_UNIT
  const displayUnit = `${quantityUnit}/${surfaceUnit}`
  return displayUnit
}

export const getConversionRatio = (displayUnitConfig) => {
  const quantityConversionRatio = displayUnitConfig?.quantity?.ratio_to_kilogram || 1
  const surfaceConversionRatio = displayUnitConfig?.surface?.ratio_to_hectare || 1
  return quantityConversionRatio * surfaceConversionRatio
}

export const convertResults = (results, displayUnitConfig) => {
  const conversionRatio = getConversionRatio(displayUnitConfig)
  const displayUnit = getDisplayUnit(displayUnitConfig)
  return results.map(result => ({
    ...result,
    quantity: result.quantity !== null && result.quantity !== undefined ? result.quantity * conversionRatio : null,
    quantity_min: result.quantity_min !== null && result.quantity_min !== undefined ? result.quantity_min * conversionRatio : null,
    quantity_max: result.quantity_max !== null && result.quantity_max !== undefined ? result.quantity_max * conversionRatio : null,
    display_unit: displayUnit
  }))
}
