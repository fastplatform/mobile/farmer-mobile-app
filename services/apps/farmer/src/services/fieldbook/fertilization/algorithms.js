import queryFertilicalcResultsPDF from 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/graphql/queryFertilicalcResultsPDF'
import queryVegsystResultsPDF from 'src/pages/fieldbook/fertilization/algorithms/vegsyst/graphql/queryVegsystResultsPDF'
import queryVisioneResultsPDF from 'src/pages/fieldbook/fertilization/algorithms/visione/graphql/queryVisioneResultsPDF'
import queryArcResultsPDF from 'src/pages/fieldbook/fertilization/algorithms/arc/graphql/queryArcResultsPDF'
import queryRequafertiResultsPDF from 'src/pages/fieldbook/fertilization/algorithms/requaferti/graphql/queryRequafertiResultsPDF'
import queryNavigatorF3ResultsPDF from 'src/pages/fieldbook/fertilization/algorithms/navigator-f3/graphql/queryNavigatorF3ResultsPDF'
import queryKalkulackyHnojenieResultsPDF from 'src/pages/fieldbook/fertilization/algorithms/kalkulacky-hnojenie/graphql/queryKalkulackyHnojenieResultsPDF'
import queryIcpaPmnResultsPDF from 'src/pages/fieldbook/fertilization/algorithms/icpa-pmn/graphql/queryIcpaPmnResultsPDF'

export const FERTILICALC = 'fertilicalc'
export const VEGSYST = 'vegsyst'
export const VISIONE = 'visione'
export const ARC = 'arc'
export const REQUAFERTI = 'requaferti'
export const KALKULACKY_HNOJENIE = 'kalkulacky-hnojenie'
export const NAVIGATOR_F3 = 'navigator-f3'
export const ICPA_PMN = 'icpa-pmn'

export default {
  [FERTILICALC]: {
    startPage: 'fertilicalcCropProfile',
    resultComponent: 'FertilicalcResults',
    hasNPKRecommendations: true,
    pdf: {
      query: queryFertilicalcResultsPDF,
      queryResponseDataKey: 'fertilicalc__generate_results_pdf'
    }
  },
  [VEGSYST]: {
    startPage: 'vegsystGreenhousePage',
    resultComponent: 'VegsystResultsCard',
    hasNPKRecommendations: false,
    pdf: {
      query: queryVegsystResultsPDF,
      queryResponseDataKey: 'vegsyst__generate_results_pdf'
    }
  },
  [VISIONE]: {
    startPage: 'visioneCropSelection',
    resultComponent: 'VisioneResultsCard',
    hasNPKRecommendations: true,
    pdf: {
      query: queryVisioneResultsPDF,
      queryResponseDataKey: 'visione__generate_results_pdf'
    }
  },
  [ARC]: {
    startPage: 'arcCropSelection',
    resultComponent: 'ArcResultsCard',
    hasNPKRecommendations: true,
    pdf: {
      query: queryArcResultsPDF,
      queryResponseDataKey: 'arc__generate_results_pdf'
    }
  },
  [REQUAFERTI]: {
    startPage: 'requafertiCropProfile',
    resultComponent: 'RequafertiResultsCard',
    hasNPKRecommendations: true,
    hasNewResultFormat: true,
    pdf: {
      query: queryRequafertiResultsPDF,
      queryResponseDataKey: 'requaferti__generate_results_pdf'
    },
    i18nLanguages: ['de']
  },
  [KALKULACKY_HNOJENIE]: {
    startPage: 'kalkulackyHnojenieCropProfile',
    resultComponent: 'KalkulackyHnojenieResultsCard',
    hasNPKRecommendations: true,
    hasNewResultFormat: true,
    pdf: {
      query: queryKalkulackyHnojenieResultsPDF,
      queryResponseDataKey: 'kalkulacky_hnojenie__generate_results_pdf'
    },
    controls: {
      pkDisplay: true,
      beforeAfterFertilization: true
    }
  },
  [NAVIGATOR_F3]: {
    startPage: 'navigatorF3CropProfile',
    resultComponent: 'NavigatorF3ResultsCard',
    hasNPKRecommendations: true,
    hasNewResultFormat: true,
    pdf: {
      query: queryNavigatorF3ResultsPDF,
      queryResponseDataKey: 'navigator_f3__generate_results_pdf'
    },
    controls: {
      pkDisplay: true,
      beforeAfterFertilization: true
    }
  },
  [ICPA_PMN]: {
    startPage: 'icpaPmnCropProfile',
    resultComponent: 'IcpaPmnResultsCard',
    hasNPKRecommendations: true,
    hasNewResultFormat: true,
    pdf: {
      query: queryIcpaPmnResultsPDF,
      queryResponseDataKey: 'icpa_pmn__generate_results_pdf'
    }
  }
}
