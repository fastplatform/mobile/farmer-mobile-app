
export const SOIL_TEXTURE_PROPERTY_ID = 'texture'

export const getSoilTextureOptionsByRegion = regionId => {
  switch (regionId) {
    case 'it-21':
      return USDA_SOIL_TEXTURES
    case 'es-an':
    case 'es-cl':
      return FAO_SOIL_TEXTURES
    case 'ee':
    default:
      return []
  }
}

export const getSoilTextureLabel = (soilTextureId, regionId) => {
  return getSoilTextureOptionsByRegion(regionId).find(soilTexture => soilTexture.id === soilTextureId)?.label
}

// USDA soil textures in Italian
export const USDA_SOIL_TEXTURES = [
  {
    id: 'argilloso',
    label: 'Argilloso'
  },
  {
    id: 'argilloso_limoso',
    label: 'Argilloso Limoso'
  },
  {
    id: 'argilloso_sabbioso',
    label: 'Argilloso Sabbioso'
  },
  {
    id: 'franco_sabbioso',
    label: 'Franco Sabbioso'
  },
  {
    id: 'sabbioso',
    label: 'Sabbioso'
  },
  {
    id: 'sabbioso_franco',
    label: 'Sabbioso Franco'
  },
  {
    id: 'franco',
    label: 'Franco'
  },
  {
    id: 'franco_argilloso',
    label: 'Franco Argilloso'
  },
  {
    id: 'franco_limoso',
    label: 'Franco Limoso'
  },
  {
    id: 'franco_limoso_argilloso',
    label: 'Franco Limoso Argilloso'
  },
  {
    id: 'franco_sabbioso_argilloso',
    label: 'Franco Sabbioso Argilloso'
  },
  {
    id: 'limoso',
    label: 'Limoso'
  }
]

// FAO soil textures in Spanish
export const FAO_SOIL_TEXTURES = [
  {
    id: 'SAND',
    label: 'Arenosa'
  },
  {
    id: 'LOAMY_SAND',
    label: 'Arenoso franca'
  },
  {
    id: 'SANDY_LOAM',
    label: 'Franco arenosa'
  },
  {
    id: 'LOAM',
    label: 'Franca'
  },
  {
    id: 'SILTY_LOAM',
    label: 'Franco limosa'
  },
  {
    id: 'SANDY_CLAY_LOAM',
    label: 'Franco areno arcillosa'
  },
  {
    id: 'CLAY_LOAM',
    label: 'Franco arcillosa'
  },
  {
    id: 'SILTY_CLAY_LOAM',
    label: 'Franco limo arcillosa'
  },
  {
    id: 'SANDY_CLAY',
    label: 'Arcillo arenosa'
  },
  {
    id: 'SILTY_CLAY',
    label: 'Arcillo limosa'
  },
  {
    id: 'CLAY',
    label: 'Arcillosa'
  }
]
