import store from 'src/store'
import { $gettext, $pgettext } from 'src/boot/translate'

export const DEFAULT_DISPLAY_SURFACE_UNIT = {
  name: 'hectare',
  shortName: 'ha',
  ratioToHectare: 1.0
}

export function formatSurface (areaInM2, options) {
  if (areaInM2 == null) {
    return ''
  } else {
    const displaySurfaceUnit = store.state.fastplatform.portalConfiguration.displaySurfaceUnit
    if (displaySurfaceUnit?.shortName && !isNaN(displaySurfaceUnit?.ratioToHectare) && Number(displaySurfaceUnit?.ratioToHectare) !== 0) {
      const areaInSurfaceUnit = (areaInM2 / 10000) / Number(displaySurfaceUnit.ratioToHectare)
      const areaInSurfaceUnitDisplay = areaInSurfaceUnit.toFixed(1)
      return `${areaInSurfaceUnitDisplay} ${displaySurfaceUnit.shortName}`
    } else {
      const areaInHectare = (areaInM2 / 10000)
      if (areaInHectare < 0.1 && options?.displayTinySurfaceInM2) {
        const areaInM2Display = Number(areaInM2).toFixed(1)
        return `${areaInM2Display} ${$pgettext('Parcel surface in square meter (m2)', 'm2')}`
      } else {
        const areaInHectareDisplay = areaInHectare < 0.1 ? '< 0.1' : areaInHectare.toFixed(1)
        return `${areaInHectareDisplay} ${$pgettext('Parcel surface in square meter (ha)', 'ha')}`
      }
    }
  }
}

export function getSurfaceConversionRatio () {
  const displaySurfaceUnit = store.state.fastplatform.portalConfiguration.displaySurfaceUnit
  return displaySurfaceUnit?.ratioToHectare || DEFAULT_DISPLAY_SURFACE_UNIT.ratioToHectare
}

export function getSurfaceDisplayUnit () {
  const displaySurfaceUnit = store.state.fastplatform.portalConfiguration.displaySurfaceUnit
  return displaySurfaceUnit?.shortName || DEFAULT_DISPLAY_SURFACE_UNIT.shortName
}

export function getYieldDisplayUnit () {
  return `${$gettext('kg')}/${getSurfaceDisplayUnit()}`
}

export function formatYield (yieldInKg) {
  if (yieldInKg == null) {
    return ''
  } else {
    const conversionRatio = getSurfaceConversionRatio()
    const yieldInSurfaceUnit = yieldInKg * conversionRatio
    return `${yieldInSurfaceUnit} ${getYieldDisplayUnit()}`
  }
}
