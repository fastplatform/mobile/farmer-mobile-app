// Default styles for rendering vector layers

const protectedSiteStyle = {
  fill: true,
  color: '#76fa11',
  radius: 5,
  weight: 1,
  fillColor: '#76fa11',
  fillOpacity: 0.2
}

const hydrographyStyle = {
  fill: true,
  color: '#5bccf5',
  radius: 5,
  weight: 2,
  fillColor: '#5bccf5',
  fillOpacity: 0.2
}

const soilSitesStyle = {
  fill: true,
  color: '#f7de20',
  radius: 5,
  weight: 1,
  fillColor: '#f7de20',
  fillOpacity: 0.2
}

const managementRestrictionOrRegulationZoneStyle = {
  fill: true,
  color: '#ab19ff',
  radius: 5,
  weight: 1,
  fillColor: '#ab19ff',
  fillOpacity: 0.2
}

const agriculturalPlotsStyle = {
  fill: true,
  color: '#1C9ACD',
  radius: 5,
  weight: 3,
  fillColor: '#1C9ACD',
  fillOpacity: 0.2
}

const transparentStyle = {
  fill: false,
  color: 'rgba(0,0,0,0)',
  radius: 5,
  weight: 0
}

const agriculturalBuildingsStyle = {
  fill: true,
  color: '#da12e5',
  radius: 5,
  weight: 1,
  fillColor: '#da12e5',
  fillOpacity: 0.2
}

const selectedFeatureStyle = {
  color: '#ff8f00',
  fillColor: '#ff8f00'
}

const highlightedFeatureStyle = {
  color: '#F23400',
  fillColor: '#F23400'
}

export {
  protectedSiteStyle,
  hydrographyStyle,
  soilSitesStyle,
  managementRestrictionOrRegulationZoneStyle,
  agriculturalPlotsStyle,
  transparentStyle,
  agriculturalBuildingsStyle,
  selectedFeatureStyle,
  highlightedFeatureStyle
}
