import { $gettext, $pgettext } from 'src/boot/translate'

import {
  protectedSiteStyle,
  hydrographyStyle,
  soilSitesStyle,
  managementRestrictionOrRegulationZoneStyle,
  agriculturalPlotsStyle,
  agriculturalBuildingsStyle,
  transparentStyle,
  highlightedFeatureStyle
} from 'src/services/map/layers/layer-styles'
import {
  getPlotLayerPopUpDefaultContent,
  getPlotLayerPopUpAsynchronousContent,
  getAgriculturalPlotsPopupContent
} from 'src/services/map/layers/layer-popups'
import { getHoldingVectorTilesUrl } from 'src/services/map/layers/layer-factory'
import store from 'src/store'
import featureFlags from 'src/services/feature-flags'

export function getDefaultBaseLayersConfig () {
  // return the default base layers to use in the maps
  return []
}

export function getProtectedSiteNatura2000Layer (display = false) {
  return {
    title: $gettext('Protected sites (Natura2000)'),
    url: store.state.fastplatform.config.vectorTilesServers.external +
      '/public.protected_site_function_source/{z}/{x}/{y}.pbf',
    type: 'vector_tile',
    style: protectedSiteStyle,
    tile_layer: 'protected_site',
    display,
    overlayPopUpContent: function ({ layer, feature }) {
      let popUpContent = ''
      let properties = layer.properties
      if (properties == null || Object.keys(properties).length === 0) {
        // No properties attached to the layer, get the feature properties
        properties = feature ? feature.properties : {}
      }
      if (properties != null) {
        const name = properties?.site_name
        popUpContent += `<div style="margin-bottom: 10px; max-width: min(70vw, 250px); min-width: min(40vw, 150px);"><div class="text-grey text-capitalize">${$gettext('Protected sites (Natura2000)')}</div><div>${name || ''}</div></div>`
        // popUpContent += '<svg focusable="false" fill="currentColor" width="20px" height="20px" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" class="q-spinner text-primary"><circle cx="15" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="60" cy="15" r="9" fill-opacity=".3"><animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from=".5" to=".5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="105" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle></svg>'
      } else {
        popUpContent += `<div class="text-body1">${$gettext('No data available')}</div>`
      }
      return popUpContent
    }
  }
}

export function getRegulationZoneNVZLayer (display = false) {
  return {
    title: $gettext('Regulation zones (NVZ)'),
    url: store.state.fastplatform.config.vectorTilesServers.external +
      '/public.management_restriction_or_regulation_zone_function_source/{z}/{x}/{y}.pbf',
    type: 'vector_tile',
    style: managementRestrictionOrRegulationZoneStyle,
    tile_layer: 'management_restriction_or_regulation_zone',
    display,
    overlayPopUpContent: function ({ layer, feature }) {
      let popUpContent = ''
      let properties = layer.properties
      if (properties == null || Object.keys(properties).length === 0) {
        // No properties attached to the layer, get the feature properties
        properties = feature ? feature.properties : {}
      }
      if (properties != null) {
        const name = properties?.name
        popUpContent += `<div style="margin-bottom: 10px; max-width: min(70vw, 250px); min-width: min(40vw, 150px);"><div class="text-grey text-capitalize">${$gettext('Regulation zones (NVZ)')}</div><div>${name || ''}</div></div>`
        // popUpContent += '<svg focusable="false" fill="currentColor" width="20px" height="20px" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" class="q-spinner text-primary"><circle cx="15" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="60" cy="15" r="9" fill-opacity=".3"><animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from=".5" to=".5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="105" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle></svg>'
      } else {
        popUpContent += `<div class="text-body1">${$gettext('No data available')}</div>`
      }
      return popUpContent
    }
  }
}

export function getHydrographySurfaceWaterLayer (display = false) {
  return {
    title: $gettext('Hydrography (surface water)'),
    url: store.state.fastplatform.config.vectorTilesServers.external +
      '/public.surface_water_function_source/{z}/{x}/{y}.pbf',
    type: 'vector_tile',
    style: hydrographyStyle,
    tile_layer: 'surface_water',
    display,
    overlayPopUpContent: function ({ layer, feature }) {
      let popUpContent = ''
      let properties = layer.properties
      if (properties == null || Object.keys(properties).length === 0) {
        // No properties attached to the layer, get the feature properties
        properties = feature ? feature.properties : {}
      }
      if (properties != null) {
        const name = properties?.geographical_name
        popUpContent += `<div style="margin-bottom: 10px; max-width: min(70vw, 250px); min-width: min(40vw, 150px);"><div class="text-grey text-capitalize">${$gettext('Hydrography (surface water)')}</div><div>${name || ''}</div></div>`
        // popUpContent += '<svg focusable="false" fill="currentColor" width="20px" height="20px" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" class="q-spinner text-primary"><circle cx="15" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="60" cy="15" r="9" fill-opacity=".3"><animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from=".5" to=".5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="105" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle></svg>'
      } else {
        popUpContent += `<div class="text-body1">${$gettext('No data available')}</div>`
      }
      return popUpContent
    },
    minZoom: 11
  }
}

export function getHydrographyWaterCourseLayer (display = false) {
  return {
    title: $gettext('Hydrography (water courses)'),
    url: store.state.fastplatform.config.vectorTilesServers.external +
      '/public.water_course_function_source/{z}/{x}/{y}.pbf',
    type: 'vector_tile',
    style: hydrographyStyle,
    tile_layer: 'water_course',
    display,
    overlayPopUpContent: function ({ layer, feature }) {
      let popUpContent = ''
      let properties = layer.properties
      if (properties == null || Object.keys(properties).length === 0) {
        // No properties attached to the layer, get the feature properties
        properties = feature ? feature.properties : {}
      }
      if (properties != null) {
        const name = properties?.geographical_name
        popUpContent += `<div style="margin-bottom: 10px; max-width: min(70vw, 250px); min-width: min(40vw, 150px);"><div class="text-grey text-capitalize">${$gettext('Hydrography (water courses)')}</div><div>${name || ''}</div></div>`
        // popUpContent += '<svg focusable="false" fill="currentColor" width="20px" height="20px" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" class="q-spinner text-primary"><circle cx="15" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="60" cy="15" r="9" fill-opacity=".3"><animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from=".5" to=".5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="105" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle></svg>'
      } else {
        popUpContent += `<div class="text-body1">${$gettext('No data available')}</div>`
      }
      return popUpContent
    },
    minZoom: 11
  }
}

export function getSoilPublicSamplesLayer (display = false) {
  return {
    title: $gettext('Soil (public samples)'),
    url: store.state.fastplatform.config.vectorTilesServers.external +
      '/public.soil_site_function_source/{z}/{x}/{y}.pbf',
    type: 'vector_tile',
    style: soilSitesStyle,
    tile_layer: 'soil_site',
    display
  }
}

export function getAgriculturalPlotsLayer (display = false) {
  return {
    title: $pgettext('Plots of agricultural land', 'Agricultural plots (all)'),
    url: store.state.fastplatform.config.vectorTilesServers.external +
      '/public.agri_plot_function_source/{z}/{x}/{y}.pbf',
    type: 'vector_tile',
    style: agriculturalBuildingsStyle,
    tile_layer: 'agri_plot',
    display,
    overlayPopUpContent: getAgriculturalPlotsPopupContent,
    minZoom: 13
  }
}

export function getHoldingCampaignPlotsLayer (display = false, filters) {
  return {
    title: $pgettext('Plots of agricultural land', 'Current campaign plots'),
    url: getHoldingVectorTilesUrl(store.state.fastplatform.tokens.accessToken, filters),
    type: 'vector_tile',
    style: agriculturalPlotsStyle,
    tile_layer: 'plot',
    display,
    overlayPopUpContent: getPlotLayerPopUpDefaultContent,
    overlayPopUpAsynchronousContent: getPlotLayerPopUpAsynchronousContent
  }
}
/**
 * Get the default overlay layers options.
 */
export function getDefaultOverlayLayersConfig (options) {
  // return the default base layers to use in the maps
  if (options) {
    const baseLayers = []

    if (featureFlags.canDisplayExternalProtectedSites()) {
      if (options.displayProtectedSiteNatura2000) {
        baseLayers.push(getProtectedSiteNatura2000Layer(options.displayProtectedSiteNatura2000))
      } else {
        baseLayers.push(getProtectedSiteNatura2000Layer())
      }
    }
    if (featureFlags.canDisplayExternalManagementRestrictionOrRegulationZone()) {
      if (options.displayRegulationZoneNVZ) {
        baseLayers.push(getRegulationZoneNVZLayer(options.displayRegulationZoneNVZ))
      } else {
        baseLayers.push(getRegulationZoneNVZLayer())
      }
    }
    if (featureFlags.canDisplayExternalSurfaceWater()) {
      if (options.displayHydrographySurfaceWater) {
        baseLayers.push(getHydrographySurfaceWaterLayer(options.displayHydrographySurfaceWater))
      } else {
        baseLayers.push(getHydrographySurfaceWaterLayer())
      }
    }
    if (featureFlags.canDisplayExternalWaterCourse()) {
      if (options.displayHydrographyWaterCourse) {
        baseLayers.push(getHydrographyWaterCourseLayer(options.displayHydrographyWaterCourse))
      } else {
        baseLayers.push(getHydrographyWaterCourseLayer())
      }
    }
    if (featureFlags.canDisplayExternalAgriPlots()) {
      if (options.displayAgriculturalPlots) {
        baseLayers.push(getAgriculturalPlotsLayer(options.displayAgriculturalPlots))
      } else {
        baseLayers.push(getAgriculturalPlotsLayer())
      }
    }

    return baseLayers
  } else {
    return [
      getProtectedSiteNatura2000Layer(),
      getRegulationZoneNVZLayer(),
      getHydrographySurfaceWaterLayer(),
      getHydrographyWaterCourseLayer(),
      getAgriculturalPlotsLayer()
    ]
  }
}

export const getOverridenOverlayConfig = (overlayConfig, additionalConfiguration) => {
  const mapOverlayConfig = { ...overlayConfig }
  if (additionalConfiguration?.attribution != null) {
    mapOverlayConfig.attribution = additionalConfiguration.attribution
  }
  if (!mapOverlayConfig.style) {
    mapOverlayConfig.style = {}
  }
  if (additionalConfiguration?.fill != null) {
    mapOverlayConfig.style.fill = additionalConfiguration.fill
  }
  if (additionalConfiguration?.color != null) {
    mapOverlayConfig.style.color = additionalConfiguration.color
  }
  if (additionalConfiguration?.radius != null) {
    mapOverlayConfig.style.radius = additionalConfiguration.radius
  }
  if (additionalConfiguration?.weight != null) {
    mapOverlayConfig.style.weight = additionalConfiguration.weight
  }
  if (additionalConfiguration?.fillColor != null) {
    mapOverlayConfig.style.fillColor = additionalConfiguration.fillColor
  }
  if (additionalConfiguration?.fillOpacity != null) {
    mapOverlayConfig.style.fillOpacity = additionalConfiguration.fillOpacity
  }
  // override style for highlighted plots
  if (overlayConfig.highlighted) {
    mapOverlayConfig.style = { ...mapOverlayConfig.style, ...highlightedFeatureStyle }
  }
  // override style to add dynamic styling for hiding plots
  if (overlayConfig.hideCurrentPlot && overlayConfig.currentPlotId != null) {
    mapOverlayConfig.style = properties => {
      if (overlayConfig.currentPlotId === properties?.id) {
        return transparentStyle
      } else {
        return overlayConfig.style
      }
    }
  }
  // override style to add dynamic styling for hiding other features than the selected feature
  if (overlayConfig.hideOtherFeatures && overlayConfig.selectedFeatureId != null) {
    mapOverlayConfig.style = properties => {
      if (overlayConfig.selectedFeatureId === properties?.id) {
        return overlayConfig.style
      } else {
        return transparentStyle
      }
    }
  }
  return mapOverlayConfig
}
