import * as L from 'leaflet'

import { selectedFeatureStyle } from 'src/services/map/layers/layer-styles'
import { getDefaultOverlayPopUpContent } from 'src/services/map/layers/layer-popups'
import store from 'src/store'

const DEFAULT_CRS = 'EPSG3857'

export const buildTileLayer = (config, pane) => {
  const layerUrlTemplate = getTileLayerUrlTemplate(config)
  return L.tileLayer(layerUrlTemplate, {
    tms: Boolean(config.inverted),
    opacity: 1,
    attribution: config.attribution || '',
    minZoom: config.minZoom,
    maxZoom: config.maxZoom,
    maxNativeZoom: config.maxNativeZoom ?? config.maxNativeZoom,
    pane
  })
}

export const buildGeoJsonOverlay = (overlayConfig, map, popupContentOptions) => {
  const overlay = L.geoJSON(overlayConfig.data, {
    pane: 'markers',
    interactive: true,
    attribution: overlayConfig.attribution,
    minZoom: overlayConfig.minZoom,
    maxZoom: overlayConfig.maxZoom,
    maxNativeZoom: overlayConfig.maxNativeZoom ?? overlayConfig.maxZoom,
    onEachFeature: (feature, layer) => {
      const popup = L.popup()
      const getPopupContent = layer => {
        if (overlayConfig.overlayPopUpContent !== null) {
          return overlayConfig.overlayPopUpContent({ feature, layer }, popupContentOptions)
        } else {
          return getDefaultOverlayPopUpContent({ feature, layer })
        }
      }
      popup.setContent(getPopupContent)
      layer.bindPopup(popup)
      layer.on('popupopen', e => {
        map.layersControl?._collapse()
        const featureId = feature?.properties?.id
        if (featureId) {
          overlay.setStyle(f => {
            let featureStyle
            if (typeof overlayConfig.style === 'function') {
              featureStyle = overlayConfig.style(f)
            } else {
              featureStyle = overlayConfig.style
            }
            if (f.properties?.id === featureId) {
              featureStyle = { ...featureStyle, ...selectedFeatureStyle }
            }
            return featureStyle
          })
        }
        if (overlayConfig.overlayPopUpAsynchronousContent === 'function') {
          e.popup.activeLayerId = featureId
          const popUpContent = overlayConfig.overlayPopUpAsynchronousContent({ feature, layer }, popupContentOptions)
          if (popUpContent instanceof Promise) {
            popUpContent.then((content) => {
              if (e.popup?.activeLayerId && feature.properties?.id === e.popup.activeLayerId) {
                // the active layer may have changed during the necessary time to get the content.
                // Display the content only if the active layer is still the same
                e.popup.setContent(content)
              }
            })
          } else {
            // if there is an error, keep the default content
            e.popup.setContent(popUpContent)
          }
        }
      })
      layer.on('popupclose', e => {
        overlay.resetStyle()
        if (overlayConfig.overlayPopUpAsynchronousContent === 'function') {
          // set the content back to the default one when the pop up is closed.
          // Required if the content was changed like, for instance, in the popupopen event.
          e.popup.setContent(getPopupContent)
        }
      })
    },
    style: feature => {
      if (typeof overlayConfig.style === 'function') {
        return overlayConfig.style(feature)
      } else {
        return overlayConfig.style
      }
    }
  })
  return overlay
}

export const buildVectorTileOverlay = (overlayConfig, map, popupContentOptions) => {
  const overlay = L.vectorGrid.protobuf(overlayConfig.url, {
    interactive: true,
    vectorTileLayerStyles: {
      [overlayConfig.tile_layer]: overlayConfig.style
    },
    pane: overlayConfig.highlighted ? 'map_highlighted_overlays' : 'map_overlays',
    attribution: overlayConfig.attribution,
    minZoom: overlayConfig.minZoom,
    maxZoom: overlayConfig.maxZoom,
    maxNativeZoom: overlayConfig.maxNativeZoom ?? overlayConfig.maxZoom,
    getFeatureId: function (f) {
      return f.properties?.id
    }
  })

  const popup = L.popup()
  const getPopupContent = layer => {
    if (overlayConfig.overlayPopUpContent !== null) {
      return overlayConfig.overlayPopUpContent({ layer }, popupContentOptions)
    } else {
      return getDefaultOverlayPopUpContent({ layer })
    }
  }
  popup.setContent(getPopupContent)
  overlay.bindPopup(popup)
  const onPopupOpen = e => {
    map.layersControl?._collapse()
    const layer = e.propagatedFrom
    const featureId = layer?.properties?.id
    if (featureId) {
      let featureStyle = overlayConfig.style
      if (typeof overlayConfig.style === 'function') {
        featureStyle = overlayConfig.style(layer.properties, map?.getZoom())
      }
      featureStyle = { ...featureStyle, ...selectedFeatureStyle }
      overlay.setFeatureStyle(featureId, featureStyle)
    }
    if (typeof overlayConfig.overlayPopUpAsynchronousContent === 'function') {
      e.popup.activeLayerId = featureId
      const popUpContent = overlayConfig.overlayPopUpAsynchronousContent({ layer }, popupContentOptions)
      if (popUpContent instanceof Promise) {
        popUpContent.then((content) => {
          if (e.popup?.activeLayerId && layer.properties?.id === e.popup.activeLayerId) {
            // the active layer may have changed during the necessary time to get the content.
            // Display the content only if the active layer is still the same
            e.popup.setContent(content)
          }
        })
      } else {
        // if there is an error, keep the default content
        e.popup.setContent(popUpContent)
      }
    }
  }
  overlay.on('popupopen', onPopupOpen)
  const onPopupClose = e => {
    const featureId = e.propagatedFrom?.properties?.id
    if (featureId) {
      overlay.resetFeatureStyle(featureId)
    }
    if (overlay.overlayPopUpAsynchronousContent === 'function') {
      // set the content back to the default one when the pop up is closed.
      // Required if the content was changed like, for instance, in the popupopen event.
      e.popup.setContent(getPopupContent)
    }
  }
  overlay.on('popupclose', onPopupClose)
  return overlay
}

export function getTileLayerUrlTemplate (layerConfig) {
  const mapServerTileServiceUrl = store.state.fastplatform.config.mapServer.tileService
  return `${mapServerTileServiceUrl}/${layerConfig.id}_${DEFAULT_CRS}/${DEFAULT_CRS}/{z}/{x}/{y}.png`
}

export function getHoldingVectorTilesUrl (accessToken, { holdingId, holdingCampaignId, siteId, plotId }) {
  let url = store.state.fastplatform.config.vectorTilesServers.fastplatform + '/public.plot_function_source/{z}/{x}/{y}.pbf'
  url += `?p_access_token=${accessToken}`
  if (holdingId) {
    url += `&p_holding_id=${holdingId}`
  }
  if (holdingCampaignId) {
    url += `&p_holding_campaign_id=${holdingCampaignId}`
  }
  if (siteId) {
    url += `&p_site_id=${siteId}`
  }
  if (plotId) {
    url += `&p_plot_id=${plotId}`
  }
  return url
}
