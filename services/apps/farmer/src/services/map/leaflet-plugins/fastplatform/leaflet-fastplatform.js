import * as L from 'leaflet'

// FastCatalogue
const CHEVRON_LEFT_ICON = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAAAXNSR0IArs4c6QAAAHhlWElmTU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUAAAABAAAARgEoAAMAAAABAAIAAIdpAAQAAAABAAAATgAAAAAAAABIAAAAAQAAAEgAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAABqgAwAEAAAAAQAAABoAAAAAf0aCPQAAAAlwSFlzAAALEwAACxMBAJqcGAAAAIZJREFUSA3tlcsNgDAMQysYgSH4dBWm5LcasEVdCV84gmwONJKVW14cpU0IJf4wgcZhMgJyQqMSNlyQCblSgSyQHt0f0Kx0QsiihHQovkMZUkOSIGRFdRmkRfHsZHsDka0lmnoUltGxM8Kky0CYZb3vMOmDJczyBX0Cs5wJOrMcPsJK1k0gAWLdGTCiIUUnAAAAAElFTkSuQmCC'
const SATELLITE_ICON = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAMSWlDQ1BJQ0MgUHJvZmlsZQAASImVVwdYU8kWnltSSWiBUKSE3kQRpEsJoUUQkA42QhJIKDEmBBE7sqyCaxcRUFd0VcRF1wLIWrGXRbH3hwWVlXWxYEPlTQro6vfe+975vrn3z5lz/lMy994ZAHRqeVJpHqoLQL6kQBYfEcJKTUtnkR4BBBABBRgCjMeXS9lxcdEAyuD9n/LmGrSGctlVyfX9/H8VPYFQzgcAiYM4UyDn50O8FwC8lC+VFQBA9IF6m+kFUiWeCLGBDCYIsVSJs9W4VIkz1bhKZZMYz4F4BwBkGo8nywZAuwXqWYX8bMijfQNiN4lALAFAhwxxIF/EE0AcCfHw/PypSgztgGPmVzzZ/+DMHOLk8bKHsLoWlZBDxXJpHm/G/9mO/y35eYrBGPZw0ESyyHhlzbBvN3KnRikxDeIeSWZMLMT6EL8TC1T2EKNUkSIySW2PmvHlHNgzwITYTcALjYLYDOJwSV5MtEafmSUO50IMVwhaJC7gJmp8FwrlYQkazlrZ1PjYQZwl47A1vo08mSqu0v64IjeJreG/IRJyB/lfF4sSU9Q5Y9RCcXIMxNoQM+W5CVFqG8y2WMSJGbSRKeKV+dtC7CeURISo+bHJWbLweI29LF8+WC+2UCTmxmhwdYEoMVLDs4PPU+VvDHGLUMJOGuQRylOjB2sRCEPD1LVjF4WSJE29WKe0ICRe4/tSmhenscepwrwIpd4aYjN5YYLGFw8sgAtSzY/HSAviEtV54pk5vLFx6nzwIhANOCAUsIACjkwwFeQAcXtPcw/8pZ4JBzwgA9lACFw1mkGPFNWMBF4TQDH4CyIhkA/5hahmhaAQ6j8NadVXV5Clmi1UeeSCxxDngyiQB38rVF6SoWjJ4BHUiL+Lzoe55sGhnPtex4aaaI1GMcjL0hm0JIYRQ4mRxHCiE26KB+L+eDS8BsPhjvvgvoPZfrEnPCZ0EB4QrhI6CTeniEtk39TDAuNAJ4wQrqk58+uacXvI6omH4AGQH3LjTNwUuOKjYSQ2HgRje0ItR5O5svpvuf9Rw1dd19hR3CgoxYgSTHH81lPbWdtziEXZ0687pM41c6ivnKGZb+Nzvuq0AN6jvrXEFmJ7sFPYUewMdgBrBizsMNaCnccOKvHQKnqkWkWD0eJV+eRCHvF38XiamMpOyt0a3LrdPqrnCoRFyvcj4EyVzpCJs0UFLDZ88wtZXAl/xHCWu5u7GwDK74j6NfWKqfo+IMyzX3QlrwEIEAwMDBz4oouGz/TeHwCgPv6iczgEXwdGAJyu4CtkhWodrrwQABXowCfKBFgAG+AI63EHXsAfBIMwMBbEgkSQBibDLovgepaB6WAWmA/KQAVYBlaDarABbALbwK9gN2gGB8BRcBKcAxfBVXAbrp4u8Az0gjegH0EQEkJHGIgJYonYIS6IO+KDBCJhSDQSj6QhGUg2IkEUyCxkAVKBrECqkY1IPfIbsh85ipxBOpCbyH2kG3mJfEAxlIYaoOaoPToS9UHZaBSaiE5Cs9FpaDFaii5Bq9A6dAfahB5Fz6FX0U70GdqHAUwLY2JWmCvmg3GwWCwdy8Jk2BysHKvE6rBGrBX+z5exTqwHe48TcQbOwl3hCo7Ek3A+Pg2fgy/Gq/FteBN+HL+M38d78c8EOsGM4ELwI3AJqYRswnRCGaGSsIWwj3ACPk1dhDdEIpFJdCB6w6cxjZhDnElcTFxH3Ek8QuwgPiT2kUgkE5ILKYAUS+KRCkhlpLWkHaTDpEukLtI7shbZkuxODienkyXkEnIleTv5EPkS+Qm5n6JLsaP4UWIpAsoMylLKZkor5QKli9JP1aM6UAOoidQc6nxqFbWReoJ6h/pKS0vLWstXa7yWWGueVpXWLq3TWve13tP0ac40Dm0iTUFbQttKO0K7SXtFp9Pt6cH0dHoBfQm9nn6Mfo/+TpuhPUKbqy3Qnqtdo92kfUn7uQ5Fx06HrTNZp1inUmePzgWdHl2Krr0uR5enO0e3Rne/7nXdPj2G3ii9WL18vcV62/XO6D3VJ+nb64fpC/RL9TfpH9N/yMAYNgwOg89YwNjMOMHoMiAaOBhwDXIMKgx+NWg36DXUNxxtmGxYZFhjeNCwk4kx7ZlcZh5zKXM38xrzg5G5EdtIaLTIqNHoktFb42HGwcZC43LjncZXjT+YsEzCTHJNlps0m9w1xU2dTcebTjddb3rCtGeYwTD/Yfxh5cN2D7tlhpo5m8WbzTTbZHberM/cwjzCXGq+1vyYeY8F0yLYIsdilcUhi25LhmWgpdhyleVhyz9Zhiw2K49VxTrO6rUys4q0UlhttGq36rd2sE6yLrHeaX3XhmrjY5Nls8qmzabX1tJ2nO0s2wbbW3YUOx87kd0au1N2b+0d7FPsf7Rvtn/qYOzAdSh2aHC440h3DHKc5ljneMWJ6OTjlOu0zumiM+rs6SxyrnG+4IK6eLmIXda5dAwnDPcdLhleN/y6K82V7Vro2uB6fwRzRPSIkhHNI56PtB2ZPnL5yFMjP7t5uuW5bXa7PUp/1NhRJaNaR710d3bnu9e4X/Gge4R7zPVo8Xgx2mW0cPT60Tc8GZ7jPH/0bPP85OXtJfNq9Or2tvXO8K71vu5j4BPns9jntC/BN8R3ru8B3/d+Xn4Ffrv9/vZ39c/13+7/dIzDGOGYzWMeBlgH8AI2BnQGsgIzAn8O7AyyCuIF1QU9CLYJFgRvCX7CdmLnsHewn4e4hchC9oW85fhxZnOOhGKhEaHloe1h+mFJYdVh98Ktw7PDG8J7IzwjZkYciSRERkUuj7zONefyufXc3rHeY2ePPR5Fi0qIqo56EO0cLYtuHYeOGztu5bg7MXYxkpjmWBDLjV0ZezfOIW5a3O/jiePjxteMfxw/Kn5W/KkERsKUhO0JbxJDEpcm3k5yTFIktSXrJE9Mrk9+mxKasiKlM3Vk6uzUc2mmaeK0lnRSenL6lvS+CWETVk/omug5sWzitUkOk4omnZlsOjlv8sEpOlN4U/ZkEDJSMrZnfOTF8up4fZnczNrMXj6Hv4b/TBAsWCXoFgYIVwifZAVkrch6mh2QvTK7WxQkqhT1iDniavGLnMicDTlvc2Nzt+YO5KXk7cwn52fk75foS3Ilx6daTC2a2iF1kZZJO6f5TVs9rVcWJdsiR+ST5C0FBnDDfl7hqPhBcb8wsLCm8N305Ol7ivSKJEXnZzjPWDTjSXF48S8z8Zn8mW2zrGbNn3V/Nnv2xjnInMw5bXNt5pbO7ZoXMW/bfOr83Pl/lLiVrCh5vSBlQWupeem80oc/RPzQUKZdJiu7/qP/jxsW4gvFC9sXeSxau+hzuaD8bIVbRWXFx8X8xWd/GvVT1U8DS7KWtC/1Wrp+GXGZZNm15UHLt63QW1G84uHKcSubVrFWla96vXrK6jOVoys3rKGuUazprIquallru3bZ2o/VouqrNSE1O2vNahfVvl0nWHdpffD6xg3mGyo2fPhZ/PONjREbm+rs6yo3ETcVbnq8OXnzqV98fqnfYrqlYsunrZKtndvitx2v966v3262fWkD2qBo6N4xccfFX0N/bWl0bdy4k7mzYhfYpdj1528Zv13bHbW7bY/Pnsa9dntr9zH2lTchTTOaeptFzZ0taS0d+8fub2v1b933+4jftx6wOlBz0PDg0kPUQ6WHBg4XH+47Ij3SczT76MO2KW23j6Ueu3J8/PH2E1EnTp8MP3nsFPvU4dMBpw+c8Tuz/6zP2eZzXueaznue3/eH5x/72r3amy54X2i56HuxtWNMx6FLQZeOXg69fPIK98q5qzFXO64lXbtxfeL1zhuCG09v5t18cavwVv/teXcId8rv6t6tvGd2r+5fTv/a2enVefB+6P3zDxIe3H7If/jskfzRx67Sx/THlU8sn9Q/dX96oDu8++KfE/7seiZ91t9T9pfeX7XPHZ/v/Tv47/O9qb1dL2QvBl4ufmXyauvr0a/b+uL67r3Jf9P/tvydybtt733en/qQ8uFJ//SPpI9Vn5w+tX6O+nxnIH9gQMqT8VRbAQwONCsLgJdbAaCnAcC4CPcPE9TnPJUg6rOpCoH/hNVnQZV4AdAIb8rtOucIALvgsJ8HuYMBUG7VE4MB6uExNDQiz/JwV3PR4ImH8G5g4JU5AKRWAD7JBgb61w0MfNoMk70JwJFp6vOlUojwbPBzsBJdNU7dCL6RfwO6/39TC78ydgAAAAlwSFlzAAAWJQAAFiUBSVIk8AAAAZtpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iPgogICAgICAgICA8ZXhpZjpQaXhlbFhEaW1lbnNpb24+NjA8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8ZXhpZjpQaXhlbFlEaW1lbnNpb24+NjA8L2V4aWY6UGl4ZWxZRGltZW5zaW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KUNJMSAAAABxpRE9UAAAAAgAAAAAAAAAeAAAAKAAAAB4AAAAeAAAC9SlLAu8AAALBSURBVGgF7Ne7buJAFAZgojQ0ROKeAIGGClFT8QhRqqRLBwIhokCHkJCoaID3SJuGJiWpaMgFEu5QRFAjChokzu6P5BXGBtszJptlcyRkGM+cmc8zHpsj+h2G/yiOfsAHPts/M3zgE2ww4B7mieVySblcjprNJk+aL2vLBQY2Eolglyer1Ur1ev3LBs7aETN4HQswPhaL5dujmcDARqPRFVLACkegn5+fWSdg7+00g4GNxWKyWAGN5f3y8rL3wbN0oAkMbDwel2CNRqOkDOjX11eWMe21jWowsIlEQgI7PT2lj48PSqfTknM2m43e3t72CtCaXDU4mUxKQMC2Wq0/ff4LaFXg29tbRayglkPb7XZqNBpClb96VATf3d2pxgoSObTD4fgWLyc7wfl8XoLFBoV7Vim2od/f35Wa7vX8TnC326Xz83MJGhg1IYd2Op2qLpia/Cx1doKRcDQakc/nOxi0IlhAe71eXdHY4e/v76lQKNDV1RUFg0G6vLwk3EYPDw80m81YJlCxjSowsgyHQ9IbLbyZyR09Hg9VKhVFgNYKqsFIPBgMuO7pVColWSVy2PWym5sbmk6nWl1b62sCI0u/3ydc/fVB4bvSRrZYLFZLd7Odmt8XFxeENz09QjOYBb0Ne3JyQtlslmq1Go3HY6pWq1QsFslsNksuaLlc1sNLTGD03Ov1yO12Swa2OdPbsH6/f/UEkFMAHw6HRblNJhPN53O56prKmMHoBc9pl8slGtj68t6GxWMOj7tdMZlMJDONXZ03uMDovNPpyKKxQeFxs3mPqsEKqFKpJGp/fX0tnGI+coPRc7vdprOzM9HgNqH4rQWLvE9PT6KcWOa8oQsYgwAaLxNyUBYscn5+foryBQIBFHOFbmCMAn/2j4+PRYNkxSLf4+OjKFcoFEIxV+gG5tmgtgkymYwIjL+qvPELAAD//xKc9tUAAAJMSURBVO3Yy64pQRQG4BYipu5mLsETGTESESGCxNRzmHkCz+J+GxK3l3BZZ/87qZPdqvTurqqWY5+9Ejq0rrU+SxXFIE1xuVyoUCiQYRimWzKZpO126zjLbDYjv99vGqvf7zse5/EC4/EJlce60OfzmfL5vAmLNzIYDNJoNFIpkZTAt9uNyuUyDQaDv0Wook+nkxDLPjmqaGnw9XqlUqn02QWPx6MFfTweKZfLcZ1lWHZUQUuDi8WiqTBV9OFwoGw2axoTQKwL7Xabe14WLQ0eDofk8/lMhcii9/s9ZTIZ01gMiymC6HQ63HkZtDQYRehA73Y7SqfTHAadZVjkQuhAK4FRhCo6lUrZwiIXQhWtDEYRKmi2ELGjqLPI8TVU0FrAKEYXGguUnZBFawPrRANjJ2TQWsGyaK/Xy81jt9DawU7RmLPz+ZwSicRL0K6AnaDZV896vaZ4PO462jWwXTRex2K1WlEsFpNC4w0LBALctd1ulw3/eXQVjAyi1RsbjmexXC4dozebjXBKtFotut/vplSug5HtKxobDuyyrGKxWFA0GuW6JVrIrLCiHC8BIzHQ2HBgl2UnsJB9h36GbTabT1O8DPy0AosTQEciEWGnMWdFK3uj0bAYkdT+ALAcWdNJ/NUTDoc5tGiBqtfr3Jx9LOOf7jArdjqdCtHs9zeOtVrtWyzGewswCp1MJhQKhbhOA1utVm1h3wqMYsfjMYeuVCq2sW8HZmg2p51i3xKMovE93ev1HHUW1yE8uPuYB/9N/IJ/eqt/O/zTO/wHEkQe8oIkk2kAAAAASUVORK5CYII='
const LOADING_ICON = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjAiIHdpZHRoPSI2NHB4IiBoZWlnaHQ9IjY0cHgiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB4bWw6c3BhY2U9InByZXNlcnZlIj48cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiBmaWxsPSIjRkZGRkZGIiAvPjxnPjxwYXRoIGQ9Ik0zOC41MiAzMy4zN0wyMS4zNiAxNi4yQTYzLjYgNjMuNiAwIDAgMSA1OS41LjE2djI0LjNhMzkuNSAzOS41IDAgMCAwLTIwLjk4IDguOTJ6IiBmaWxsPSIjMDAwMDAwIiBmaWxsLW9wYWNpdHk9IjEiLz48cGF0aCBkPSJNMzguNTIgMzMuMzdMMjEuMzYgMTYuMkE2My42IDYzLjYgMCAwIDEgNTkuNS4xNnYyNC4zYTM5LjUgMzkuNSAwIDAgMC0yMC45OCA4LjkyeiIgZmlsbD0iI2MwYzBjMCIgZmlsbC1vcGFjaXR5PSIwLjI1IiB0cmFuc2Zvcm09InJvdGF0ZSg0NSA2NCA2NCkiLz48cGF0aCBkPSJNMzguNTIgMzMuMzdMMjEuMzYgMTYuMkE2My42IDYzLjYgMCAwIDEgNTkuNS4xNnYyNC4zYTM5LjUgMzkuNSAwIDAgMC0yMC45OCA4LjkyeiIgZmlsbD0iI2MwYzBjMCIgZmlsbC1vcGFjaXR5PSIwLjI1IiB0cmFuc2Zvcm09InJvdGF0ZSg5MCA2NCA2NCkiLz48cGF0aCBkPSJNMzguNTIgMzMuMzdMMjEuMzYgMTYuMkE2My42IDYzLjYgMCAwIDEgNTkuNS4xNnYyNC4zYTM5LjUgMzkuNSAwIDAgMC0yMC45OCA4LjkyeiIgZmlsbD0iI2MwYzBjMCIgZmlsbC1vcGFjaXR5PSIwLjI1IiB0cmFuc2Zvcm09InJvdGF0ZSgxMzUgNjQgNjQpIi8+PHBhdGggZD0iTTM4LjUyIDMzLjM3TDIxLjM2IDE2LjJBNjMuNiA2My42IDAgMCAxIDU5LjUuMTZ2MjQuM2EzOS41IDM5LjUgMCAwIDAtMjAuOTggOC45MnoiIGZpbGw9IiNjMGMwYzAiIGZpbGwtb3BhY2l0eT0iMC4yNSIgdHJhbnNmb3JtPSJyb3RhdGUoMTgwIDY0IDY0KSIvPjxwYXRoIGQ9Ik0zOC41MiAzMy4zN0wyMS4zNiAxNi4yQTYzLjYgNjMuNiAwIDAgMSA1OS41LjE2djI0LjNhMzkuNSAzOS41IDAgMCAwLTIwLjk4IDguOTJ6IiBmaWxsPSIjYzBjMGMwIiBmaWxsLW9wYWNpdHk9IjAuMjUiIHRyYW5zZm9ybT0icm90YXRlKDIyNSA2NCA2NCkiLz48cGF0aCBkPSJNMzguNTIgMzMuMzdMMjEuMzYgMTYuMkE2My42IDYzLjYgMCAwIDEgNTkuNS4xNnYyNC4zYTM5LjUgMzkuNSAwIDAgMC0yMC45OCA4LjkyeiIgZmlsbD0iI2MwYzBjMCIgZmlsbC1vcGFjaXR5PSIwLjI1IiB0cmFuc2Zvcm09InJvdGF0ZSgyNzAgNjQgNjQpIi8+PHBhdGggZD0iTTM4LjUyIDMzLjM3TDIxLjM2IDE2LjJBNjMuNiA2My42IDAgMCAxIDU5LjUuMTZ2MjQuM2EzOS41IDM5LjUgMCAwIDAtMjAuOTggOC45MnoiIGZpbGw9IiNjMGMwYzAiIGZpbGwtb3BhY2l0eT0iMC4yNSIgdHJhbnNmb3JtPSJyb3RhdGUoMzE1IDY0IDY0KSIvPjxhbmltYXRlVHJhbnNmb3JtIGF0dHJpYnV0ZU5hbWU9InRyYW5zZm9ybSIgdHlwZT0icm90YXRlIiB2YWx1ZXM9IjAgNjQgNjQ7NDUgNjQgNjQ7OTAgNjQgNjQ7MTM1IDY0IDY0OzE4MCA2NCA2NDsyMjUgNjQgNjQ7MjcwIDY0IDY0OzMxNSA2NCA2NCIgY2FsY01vZGU9ImRpc2NyZXRlIiBkdXI9Ijk2MG1zIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSI+PC9hbmltYXRlVHJhbnNmb3JtPjwvZz48L3N2Zz4='
const NDVI_LEGEND_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAP4AAAABCAYAAAD+UhOdAAAAAXNSR0IArs4c6QAAAEJJREFUOE9jFBAQ+M/IyMiACzMwMDAwMTGBKDiNTz263Kj+gQ8/UuILX/yRYw4o/snRB9NDbrqjpv6BcD+l/ibkfwA5mAP4LjdrzwAAAABJRU5ErkJggg=='

L.Control.FastSatelliteImageryCatalogue = L.Control.extend({
  _remote_data: {
    rgb: {
      dates: [],
      layers: {},
      cloudCovers: {}
    },
    ndvi: {
      dates: [],
      layers: {},
      cloudCovers: {}
    }
  },
  _current_layers: [],
  _enabled: false,
  _mode: 'rgb',
  _map: null,
  _select_dom: null,
  _satellite_image_dom: null,
  _control_interior_span_dom: null,
  _fetch_retry_handle: null,
  _user_selected_timestamp: null,
  options: null,

  _init_options: () => {
    return {
      position: 'bottomleft',
      mode: 'rgb',
      enabled: false,
      catalogueURL: '',
      catalogueGraphQLQuery: '',
      catalogueHTTPAuthorization: '',
      uuid: '',
      pane: '',
      texts: {
        NoImages: 'No images for this area',
        less: 'less',
        more: 'more',
        Vegetation: 'Vegetation'
      },
      onShow: () => { },
      onHide: () => { }
    }
  },

  initialize: function (options) {
    // set default options if nothing is set (merge one step deep)
    const o = this._init_options()
    for (const i in options) {
      if (typeof o[i] === 'object') {
        L.extend(o[i], options[i])
      } else {
        o[i] = options[i]
      }
    }
    this.options = {
      ...JSON.parse(JSON.stringify(o)),
      onShow: () => { },
      onHide: () => { }
    }
  },

  onAdd (map) {
    this._map = map
    this._mode = this.options.mode
    this._enabled = this.options.enabled || false

    // Initialise the HTML
    this._control_div = L.DomUtil.create('div', 'leaflet-control-layers leaflet-control-layers-expanded leaflet-control fastcatalogue-control')
    this._control_div.id = 'fastcatalogue-control-' + this.options.uuid

    this._satellite_image_dom = L.DomUtil.create('div', 'fastcatalogue-control-satellite-image-container', this._control_div)
    this._satellite_image_dom.id = 'fastcatalogue-control-satellite-image-' + this.options.uuid
    this._satellite_image_img_dom = L.DomUtil.create('img', 'fastcatalogue-control-satellite-image', this._satellite_image_dom)
    this._satellite_image_img_dom.src = SATELLITE_ICON
    L.DomEvent.addListener(this._satellite_image_dom, 'click', this.onSatelliteImageClick, this)

    this._control_interior_span_dom = L.DomUtil.create('div', 'fastcatalogue-control-interior', this._control_div)
    this._control_interior_span_dom.id = 'fastcatalogue-control-interior' + this.options.uuid
    this._control_interior_span_dom.style.display = 'none'

    this._select_dom = L.DomUtil.create('select', 'fastcatalogue-control-select-date', this._control_interior_span_dom)
    this._select_dom.id = 'fastcatalogue-control-select-date-' + this.options.uuid
    this._select_dom.style.display = 'none'
    this._select_dom.disabled = true
    L.DomEvent.addListener(this._select_dom, 'change', this.onSelectDateChange, this)
    L.DomEvent.disableClickPropagation(this._select_dom)

    this._radios_inputs_dom = L.DomUtil.create('div', 'fastcatalogue-control-radio-inputs', this._control_interior_span_dom)

    this._radio_input_rgb_container_dom = L.DomUtil.create('div', 'fastcatalogue-control-radio-input-container', this._radios_inputs_dom)
    this._radio_input_rgb_dom = L.DomUtil.create('input', 'fastcatalogue-control-radio-input', this._radio_input_rgb_container_dom)
    this._radio_input_rgb_dom.id = 'fastcatalogue-control-rgb-' + this.options.uuid
    this._radio_input_rgb_dom.type = 'radio'
    this._radio_input_rgb_dom.name = 'layer'
    this._radio_input_rgb_dom.disabled = true
    this._radio_input_rgb_dom.checked = this._mode === this._radio_input_rgb_dom.id
    this._label_radio_input_rgb_dom = L.DomUtil.create('label', 'fastcatalogue-control-radio-label', this._radio_input_rgb_container_dom)
    this._label_radio_input_rgb_dom.innerHTML = 'RGB'
    this._label_radio_input_rgb_dom.for = this._radio_input_rgb_dom.id
    L.DomEvent.addListener(this._radio_input_rgb_container_dom, 'click', this.onRadioInputRGBClick, this)

    this._radio_input_ndvi_container_dom = L.DomUtil.create('div', 'fastcatalogue-control-radio-input-container', this._radios_inputs_dom)
    this._radio_input_ndvi_dom = L.DomUtil.create('input', 'fastcatalogue-control-radio-input', this._radio_input_ndvi_container_dom)
    this._radio_input_ndvi_dom.type = 'radio'
    this._radio_input_ndvi_dom.id = 'fastcatalogue-control-ndvi-' + this.options.uuid
    this._radio_input_ndvi_dom.name = 'layer'
    this._radio_input_ndvi_dom.disabled = true
    this._radio_input_ndvi_dom.checked = this._mode === this._radio_input_ndvi_dom.id
    this._label_radio_input_ndvi_dom = L.DomUtil.create('label', 'fastcatalogue-control-radio-label', this._radio_input_ndvi_container_dom)
    this._label_radio_input_ndvi_dom.innerHTML = 'NDVI'
    this._label_radio_input_ndvi_dom.for = this._radio_input_ndvi_dom.id
    L.DomEvent.addListener(this._radio_input_ndvi_container_dom, 'click', this.onRadioInputNDVIClick, this)

    this._loading_icon_dom = L.DomUtil.create('img', 'fastcatalogue-control-loading-icon', this._radios_inputs_dom)
    this._loading_icon_dom.src = LOADING_ICON
    this._loading_icon_dom.style = 'display: none;'

    this._ndvi_legend_button_dom = L.DomUtil.create('img', 'fastcatalogue-control-ndvi-legend-button fastplatform-layer-img-ndvi-legend', this._radios_inputs_dom)
    this._ndvi_legend_button_dom.src = NDVI_LEGEND_IMAGE
    this._ndvi_legend_button_dom.style = 'display: inline-block;'
    L.DomEvent.addListener(this._ndvi_legend_button_dom, 'click', this.onNDVILegendButtonClick, this)

    // The below elements as positioned absolutely, outside the main element
    this._cloud_warning_dom = L.DomUtil.create('div', 'fastcatalogue-control-cloud-warning leaflet-control-layers', this._control_div)
    this._cloud_warning_dom.style = 'display: none;'
    this._ndvi_legend_dom = L.DomUtil.create('div', 'leaflet-control-layers fastcatalogue-control-ndvi-legend', this._control_div)
    this._ndvi_legend_dom.style.display = 'none'
    this._ndvi_legend_text_left_dom = L.DomUtil.create('div', 'fastcatalogue-control-ndvi-legend-text', this._ndvi_legend_dom)
    this._ndvi_legend_text_left_dom.textContent = '⇠ ' + this.options.texts.less
    this._ndvi_legend_text_left_dom.style = 'width: 33%; text-align: left;'
    this._ndvi_legend_text_middle_dom = L.DomUtil.create('div', 'fastcatalogue-control-ndvi-legend-text', this._ndvi_legend_dom)
    this._ndvi_legend_text_middle_dom.textContent = this.options.texts.Vegetation + ' ▼'
    this._ndvi_legend_text_middle_dom.style = 'width: 33%; text-align: center; font-weight: bold; white-space: nowrap;'
    this._ndvi_legend_text_right_dom = L.DomUtil.create('div', 'fastcatalogue-control-ndvi-legend-text', this._ndvi_legend_dom)
    this._ndvi_legend_text_right_dom.textContent = this.options.texts.more + ' ⇢'
    this._ndvi_legend_text_right_dom.style = 'width: 33%; text-align: right;'
    this._ndvi_legend_img_dom = L.DomUtil.create('img', 'fastplatform-layer-img-ndvi-legend', this._ndvi_legend_dom)
    this._ndvi_legend_img_dom.src = NDVI_LEGEND_IMAGE
    this._ndvi_legend_img_dom.style = 'width: 100%; height: 10px;'
    L.DomEvent.addListener(this._ndvi_legend_text_middle_dom, 'click', this.onNDVILegendClick, this)
    L.DomEvent.addListener(this._ndvi_legend_text_middle_dom, 'touchstart', this.onNDVILegendClick, this)

    this._map.on('baselayerchange', this.onMapBaseLayerChange)
    this._map.on('moveend', this.updateRemoteData, this)

    this.setMode(this._mode)

    if (this._enabled) {
      this.activateControl()
    }

    return this._control_div
  },

  onSatelliteImageClick () {
    this._enabled = !this._enabled
    if (this._enabled) {
      this.activateControl()
    } else {
      this.deactivateControl()
    }
  },

  onSelectDateChange (event) {
    this._user_selected_timestamp = this._select_dom.value
    this.removeCurrentLayers()
    this.addLayers()
    this.populateCloudWarning()
  },

  onRadioInputRGBClick (event) {
    this.setMode('rgb')
    this.populateSelect()
    this.populateCloudWarning()
    this.updateLayers()
  },

  onRadioInputNDVIClick (event) {
    this.setMode('ndvi')
    this.populateSelect()
    this.populateCloudWarning()
    this.updateLayers()
  },

  onNDVILegendClick (event) {
    this._ndvi_legend_dom.style.display = 'none'
    // this._copernicus_logo_dom.style.display = 'block'
  },

  onNDVILegendButtonClick (event) {
    if (this._ndvi_legend_dom.style.display === 'none') {
      this._ndvi_legend_dom.style.display = 'inline-block'
    } else {
      this._ndvi_legend_dom.style.display = 'none'
    }
  },

  setMode (mode) {
    this._radio_input_rgb_dom.checked = (mode === 'rgb')
    this._radio_input_ndvi_dom.checked = (mode === 'ndvi')
    if (mode === 'rgb') {
      this._ndvi_legend_dom.style.display = 'none'
    }
    this._ndvi_legend_button_dom.style.display = (mode === 'ndvi' ? 'inline-block' : 'none')
    this._mode = mode
  },

  onRemove (map) {
    this._enabled = false

    // Remove events
    L.DomEvent.removeListener(this._satellite_image_dom, 'click', this.onSatelliteImageClick, this)
    L.DomEvent.removeListener(this._select_dom, 'change', this.onSelectDateChange, this)
    L.DomEvent.removeListener(this._radio_input_rgb_container_dom, 'click', this.onRadioInputRGBClick, this)
    L.DomEvent.removeListener(this._radio_input_ndvi_container_dom, 'click', this.onRadioInputNDVIClick, this)
    L.DomEvent.removeListener(this._ndvi_legend_button_dom, 'click', this.onNDVILegendButtonClick, this)
    L.DomEvent.removeListener(this._ndvi_legend_text_middle_dom, 'click', this.onNDVILegendClick, this)
    L.DomEvent.removeListener(this._ndvi_legend_text_middle_dom, 'touchstart', this.onNDVILegendClick, this)
    map.off('baselayerchange', this.onMapBaseLayerChange)
    map.off('moveend', this.updateRemoteData, this)

    // Remove layers
    this.removeCurrentLayers()
  },

  onMapBaseLayerChange (e) {
    if (this._enabled) {
      e.layer.bringToBack()
    }
  },

  async activateControl () {
    this._control_interior_span_dom.style.display = 'flex'
    this._select_dom.style.display = 'block'
    this._satellite_image_img_dom.src = CHEVRON_LEFT_ICON
    // populate the select with data from the list corresponding to the current mode
    await this.updateRemoteData()
    this.options.onShow()
  },

  deactivateControl () {
    this._control_interior_span_dom.style.display = 'none'
    this._select_dom.style.display = 'none'
    this._cloud_warning_dom.style.display = 'none'
    this._satellite_image_img_dom.src = SATELLITE_ICON
    this._ndvi_legend_dom.style.display = 'none'
    this.removeCurrentLayers()
    this.options.onHide()
  },

  async updateRemoteData () {
    if (!this._enabled || !this._map) {
      return
    }

    // Get the screen map bbox and format it
    const geojson = L.rectangle(this._map.getBounds()).toGeoJSON()
    geojson.geometry.crs = {
      type: 'name',
      properties: {
        name: 'urn:ogc:def:crs:EPSG::4326'
      }
    }

    const handleLoadingIcon = window.setTimeout(() => {
      this._loading_icon_dom.style = 'display: inline'
    }, 1000)

    if (this._fetch_retry_handle) {
      window.clearTimeout(this._fetch_retry_handle)
      this._fetch_retry_handle = null
    }

    try {
      const response = await fetch(this.options.catalogueURL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: this.options.catalogueHTTPAuthorization
        },
        body: JSON.stringify({
          query: this.options.catalogueGraphQLQuery,
          variables: {
            bounds: JSON.stringify(geojson.geometry)
          }
        })
      })
      const data = await response.json()

      // Process and populate the select list
      this.processConfiguration(data.data.configuration)
      this.processRemoteData(data.data.satellite_image_processing)
      this.populateSelect()
      this.populateCloudWarning()
      this.updateLayers()
      // Remove the loading icon
      window.clearTimeout(handleLoadingIcon)
      this._loading_icon_dom.style = 'display: none'
    } catch (e) {
      console.warn('Could not fetch satellite image data: retrying in 5000ms...', e)
      // Try again in 5 seconds
      this._fetch_retry_handle = window.setTimeout(async () => {
        if (this._enabled && this._map) {
          this.updateRemoteData()
        }
      }, 5000)
      // Empty the select list
      this.processRemoteData([])
      this.populateSelect()
      this.populateCloudWarning()
      this.updateLayers()
      // Remove the loading icon
      window.clearTimeout(handleLoadingIcon)
      this._loading_icon_dom.style = 'display: none'
    }
  },

  processConfiguration (configuration) {
    this._configuration = configuration[0]
  },

  processRemoteData (data) {
    this._remote_data = {
      rgb: {
        dates: [],
        layers: {},
        cloudCovers: {}
      },
      ndvi: {
        dates: [],
        layers: {},
        cloudCovers: {}
      }
    }

    for (const item of data) {
      const url = item.url
      const process = item.process
      const imageName = item.satellite_image.name
      const acquisitionDate = item.satellite_image.acquisition_date
      const cloudCover = item.satellite_image.cloud_cover

      if (process !== 'rgb' && process !== 'ndvi') {
        console.error('Unsupported process type:', process)
        continue
      }

      const arrImageName = imageName.split('_')
      const time = arrImageName[2].split('T')[1]
      const timestamp = new Date(Date.parse(acquisitionDate + 'T' + time.slice(0, 2) + ':' + time.slice(2, 4))).valueOf()

      const layer = L.tileLayer(url + '/{z}/{x}/{y}.png', {
        opacity: 1,
        minZoom: 2,
        maxZoom: 22,
        maxNativeZoom: 14,
        zIndex: 100, // to be over the other base layers
        className: process === 'ndvi' ? 'fastplatform-layer-img fastplatform-layer-img-ndvi' : 'fastplatform-layer-img',
        pane: this.options.pane,
        attribution: this._configuration?.rgb_ndvi_overlay_style?.attribution || 'Copernicus Sentinel-2'
      })

      if (!(timestamp in this._remote_data[process].layers)) {
        this._remote_data[process].dates.push(timestamp)
        this._remote_data[process].layers[timestamp] = []
        this._remote_data[process].cloudCovers[timestamp] = []
      }
      this._remote_data[process].layers[timestamp].push(layer)
      this._remote_data[process].cloudCovers[timestamp].push(cloudCover)
    }
  },

  populateSelect () {
    // Find a default element to select
    let defaultElement
    if (this._user_selected_timestamp) {
      const timestamp = this._user_selected_timestamp
      defaultElement = this._remote_data[this._mode].dates.find(x => x === timestamp)
      // if timestamp if not available in new remote date, try to find the closest available timestamp
      if (!defaultElement) {
        const firstIndexInferior = this._remote_data[this._mode].dates.findIndex(x => x < timestamp)
        if (firstIndexInferior > 0) {
          defaultElement = this._remote_data[this._mode].dates[firstIndexInferior - 1]
        } else if (firstIndexInferior === 0) {
          defaultElement = this._remote_data[this._mode].dates[firstIndexInferior]
        } else {
          defaultElement = this._remote_data[this._mode].dates[this._remote_data[this._mode].dates.length - 1]
        }
      }
    }

    // Remove all options from the list
    const lengthSelect = this._select_dom.options.length - 1
    let i
    for (i = lengthSelect; i >= 0; i--) {
      this._select_dom.remove(i)
    }

    // Add all elements to the list
    if (this._remote_data[this._mode].dates.length) {
      for (const timestamp of this._remote_data[this._mode].dates) {
        let cloudCover = this._remote_data[this._mode].cloudCovers[timestamp].reduce((a, b) => a + b, 0)
        cloudCover /= this._remote_data[this._mode].cloudCovers[timestamp].length
        cloudCover = Math.round(cloudCover, 0)

        const opt = document.createElement('option')
        const optText = (new Date(timestamp)).toLocaleDateString(navigator.languages, { dateStyle: 'medium' })
        opt.appendChild(document.createTextNode(optText + `  ☁ ${cloudCover}%`))

        // set value property of opt
        opt.value = timestamp

        // add opt to end of select box (this._select_dom)
        this._select_dom.appendChild(opt)
      }
      this._select_dom.disabled = false
      this._radio_input_rgb_dom.disabled = false
      this._radio_input_ndvi_dom.disabled = false
    } else {
      const opt = document.createElement('option')
      opt.appendChild(document.createTextNode(this.options.texts.NoImages))
      opt.value = '-'
      this._select_dom.appendChild(opt)
      this._select_dom.disabled = true
      this._radio_input_rgb_dom.disabled = true
      this._radio_input_ndvi_dom.disabled = true
    }

    // Set the selected element
    if (defaultElement) {
      this._select_dom.value = defaultElement
    } else {
      // if no selected element, select the latest available date
      if (this._select_dom.options.length > 0) {
        this._select_dom.value = this._select_dom.options[0].value
      } else {
        this._select_dom.value = null
      }
    }
  },

  populateCloudWarning () {
    if (this._select_dom.value && this._enabled && this._remote_data[this._mode].cloudCovers[this._select_dom.value]) {
      let cloudCover = this._remote_data[this._mode].cloudCovers[this._select_dom.value].reduce((a, b) => a + b, 0)
      cloudCover /= this._remote_data[this._mode].cloudCovers[this._select_dom.value].length
      cloudCover = Math.round(cloudCover, 0)
      if (cloudCover > 50) {
        this._cloud_warning_dom.textContent = `☁ ${cloudCover}%`
        const warningColor = cloudCover > 75 ? 'red' : 'orange'
        this._cloud_warning_dom.style.display = 'block'
        this._cloud_warning_dom.style.backgroundColor = warningColor
      } else {
        this._cloud_warning_dom.style.display = 'none'
      }
    } else {
      this._cloud_warning_dom.style.display = 'none'
    }
  },

  removeCurrentLayers () {
    if (this._current_layers) {
      for (const layer of this._current_layers) {
        this._map.removeLayer(layer)
      }
      this._current_layers = []
    }
  },

  addLayers () {
    this._current_layers = this._remote_data[this._mode].layers[this._select_dom.value]
    for (const layer of this._current_layers) {
      this._map.addLayer(layer)
    }
  },

  updateLayers () {
    if (this._enabled && this._remote_data[this._mode].layers[this._select_dom.value]) {
      if (this._remote_data[this._mode].layers[this._select_dom.value].length === this._current_layers.length) {
        let isDifferent = false
        for (let i = 0; i < this._current_layers.length; i++) {
          if (this._remote_data[this._mode].layers[this._select_dom.value][i]._url !== this._current_layers[i]._url) {
            isDifferent = true
            break
          }
        }
        if (isDifferent) {
          this.removeCurrentLayers()
          this.addLayers()
        }
      } else {
        this.removeCurrentLayers()
        this.addLayers()
      }
    }
  }
})

L.control.FastSatelliteImageryCatalogue = function (options) {
  return new L.Control.FastSatelliteImageryCatalogue(options)
}
