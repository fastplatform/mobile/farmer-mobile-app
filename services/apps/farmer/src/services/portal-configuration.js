import { apolloClient } from 'src/boot/apollo'
import store from 'src/store'
import queryPortalConfiguration from 'src/services/graphql/queryPortalConfiguration'

export async function loadPortalConfiguration () {
  try {
    const { data } = await apolloClient.query({
      query: queryPortalConfiguration
    })
    const config = data.configuration
    const portalConfiguration = {
      mapOverlayStyles: {
        protected_site: config.protected_site_overlay_style,
        hydrography: config.hydrography_overlay_style,
        soil: config.soil_overlay_style,
        management_restriction_or_regulation_zone: config.management_restriction_or_regulation_zone_overlay_style,
        agri_plot: config.agri_plot_overlay_style,
        plot: config.plots_overlay_style
      },
      mapDefaults: {
        centerLatitude: config.backend_leaflet_default_center_latitude,
        centerLongitude: config.backend_leaflet_default_center_longitude,
        zoom: config.backend_leaflet_default_zoom
      },
      displaySurfaceUnit: {
        name: config.display_surface_unit_name,
        shortName: config.display_surface_unit_short_name,
        ratioToHectare: config.display_surface_unit_ratio_to_hectare
      }
    }
    store.commit('fastplatform/updatePortalConfiguration', portalConfiguration)
    console.log('Portal configuration loaded', portalConfiguration)
  } catch (e) {
    console.error('Could not load portal configuration', e)
  }
}
