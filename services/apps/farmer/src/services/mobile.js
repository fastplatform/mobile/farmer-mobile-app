import { uploadServiceInstance, initializeUploadService, stopUploadService } from 'src/services/photo/upload'
import { pushNotificationServiceInstance, initializePushNotificationService, stopPushNotificationService } from 'src/services/push-notification'

/**
 * Initialize mobile services for the current user.
 */
export function initMobileServices (isIos) {
  // Register the device for push notifications
  initializePushNotificationService()
  pushNotificationServiceInstance.registerDevice()
  pushNotificationServiceInstance.checkPermissions()

  // initialize the photo upload service
  initializeUploadService(isIos)
  uploadServiceInstance.startUpload()
}

/**
 * Stop mobile services for the current user.
 */
export async function stopMobileServices () {
  // unregister the mobile device from the push notification server
  try {
    if (pushNotificationServiceInstance) {
      await pushNotificationServiceInstance.unregisterDevice()
    }
  } catch (e) {
    // The device couldn't be unregistered
    console.error('Push notification unregister error', e)
    // TODO Handle the error
  } finally {
    // remove the push notification service instance
    stopPushNotificationService()
  }

  // delete local photos and directory
  try {
    if (uploadServiceInstance) {
      if (!await uploadServiceInstance.deletePhotosAndDirectory()) {
        // The user directory (that contains the photo) was not deleted. Some photos may still exists.
        console.error('Could not delete photos directory')
        // TODO handle error
      }
    }
  } catch (e) {
    console.error('Logout photo deletion error', e)
  } finally {
    // remove the upload service instance
    stopUploadService()
  }
}
