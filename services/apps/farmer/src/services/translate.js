// **Note: Do not forget to update the "webpackInclude" comment line when a new language is added**
import { Quasar } from 'quasar'

import store from 'src/store'

export const DEFAULT_LANGUAGE = 'en'

export const AVAILABLE_LANGUAGES = {
  bg: {
    name: 'български език',
    languageCode: 'bg'
  },
  de: {
    name: 'Deutsch',
    languageCode: 'de'
  },
  el: {
    name: 'Ελληνική',
    languageCode: 'el'
  },
  en: {
    name: 'English',
    languageCode: 'en'
  },
  es: {
    name: 'Español',
    languageCode: 'es'
  },
  et: {
    name: 'Eestlane',
    languageCode: 'et'
  },
  fr: {
    name: 'Français',
    languageCode: 'fr'
  },
  it: {
    name: 'Italiano',
    languageCode: 'it'
  },
  ro: {
    name: 'Română',
    languageCode: 'ro'
  },
  sk: {
    name: 'Slovenčina',
    languageCode: 'sk'
  }
}

// Map the language code used for the translation with the quasar language iso name that is used to import language
// (for quasar component)
export const MAP_LANGUAGE_CODE_QUASAR_ISO_NAME = {
  bg: 'bg',
  de: 'de',
  el: 'el',
  en: 'en-GB',
  es: 'es',
  et: 'et',
  fr: 'fr',
  it: 'it',
  ro: 'ro',
  sk: 'sk'
}

export async function setQuasarLanguage (langCodeTwoDigit) {
  // dynamic import, so loading on demand only
  try {
    const langIsoName = MAP_LANGUAGE_CODE_QUASAR_ISO_NAME[langCodeTwoDigit]
    if (langIsoName) {
      // NOTE: webpackInclude is required to limit the number of files to be bundled improving performances.
      await import(
        /* webpackInclude: /(en-GB|de|es|et|it|fr|el|bg|ro|sk)\.js$/ */
        'quasar/lang/' + langIsoName
      )
        .then(lang => {
          Quasar.lang.set(lang.default)
        })
    } else {
      console.error(`[LANGUAGE] The language "${langCodeTwoDigit}" is not supported`)
    }
  } catch (err) {
    // Requested Quasar Language Pack does not exist,
    // let's not break the app, so catching error
    console.error('[LANGUAGE] Failed to set quasar language: ', err)
  }
}

export function getCurrentLanguage () {
  return store.state.fastplatform.preferredLanguage || DEFAULT_LANGUAGE
}

/**
 * Get the localized field name with the current language
 */
export function getI18nFieldName (fieldName) {
  return `${fieldName}_${getCurrentLanguage()}`
}

/**
 * Get the localized version of the object field (or the field itself if no translation is available)
 * Some graphQL nodes have a 'i18N' JSON field with some optional translations for some fields
 * Example for the 'lable' field in the german locale (de): { 'label_de': 'xxx' }
 */
export function getI18nField (object, fieldName) {
  const i18nFieldName = getI18nFieldName(fieldName)
  const i18n = object?.i18n?.[i18nFieldName]
  return i18n ?? object?.[fieldName]
}

/**
 * Parse the language code list from the config language_codes entry
 */
export function parseLanguageConfig (languageCodes) {
  try {
    let languages = []
    if (languageCodes) {
      if (Array.isArray(languageCodes)) {
        languages = languageCodes
      } else {
        languages = languageCodes.split(',')
      }
      languages = languages.map(code => code.trim()).filter(code => code in AVAILABLE_LANGUAGES)
    }
    if (languages.length > 0) {
      return languages
    } else {
      throw new Error('Unknown language(s)')
    }
  } catch (e) {
    console.error('Could not parse region language codes', e)
    return [DEFAULT_LANGUAGE]
  }
}
