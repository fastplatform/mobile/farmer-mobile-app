// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js
const fs = require('fs')
const packageJson = require('./package.json')

module.exports = function (ctx) {
  // Define env variables to use in the app (both for dev and prod environment)
  const env = {
    APP_VERSION: packageJson.version
  }
  return {
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    // https://quasar.dev/quasar-cli/cli-documentation/boot-files
    boot: [
      'axios',
      'capacitor',
      'apollo',
      'translate',
      'leaflet',
      'screen',
      'notify-defaults'
    ],

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-css
    css: [
      'app.scss'
    ],

    // https://github.com/quasarframework/quasar/tree/dev/extras
    extras: [
      // 'ionicons-v4',
      // 'mdi-v4',
      'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      'line-awesome'
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!
      // 'roboto-font' // optional, you are not bound to it
      // 'material-icons' // optional, you are not bound to it
    ],

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
      iconSet: 'fontawesome-v5', // Quasar icon set
      lang: 'en-GB', // Quasar language pack

      components: [],
      directives: [
        'TouchPan',
        'TouchSwipe'
      ],

      // Quasar plugins
      plugins: [
        'Loading',
        'AppFullscreen',
        'Notify',
        'Dialog'
      ],
      config: {
        capacitor: {
          // requires Quasar v1.9.3+
          backButtonExit: true // Quasar handles app exit on mobile phone back button
        }
      }
    },

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-build
    build: {
      vueRouterMode: 'history', // available values: 'hash', 'history'
      env,
      devtool: ctx.dev ? 'eval-source-map' : null,

      // rtl: false, // https://quasar.dev/options/rtl-support
      // showProgress: false,
      // gzip: true,
      // analyze: true,

      // Options below are automatically set depending on the env, set them if you want to override
      // extractCSS: false,

      // https://quasar.dev/quasar-cli/cli-documentation/handling-webpack
      extendWebpack (cfg) {
        cfg.resolve.extensions = ['.mjs', ...cfg.resolve.extensions, '.gql', '.graphql']

        cfg.module.rules.push({
          test: /\.m?js/,
          resolve: {
            fullySpecified: false
          }
        })

        cfg.module.rules.push({
          test: /\.(graphql|gql)$/,
          exclude: /node_modules/,
          loader: 'graphql-tag/loader'
        })
      }
    },

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
      https: {
        key: ctx.prod ? '' : fs.readFileSync('./certificates/localhost.fastplatform.eu/privkey1.pem'),
        cert: ctx.prod ? '' : fs.readFileSync('./certificates/localhost.fastplatform.eu/cert1.pem'),
        ca: ctx.prod ? '' : fs.readFileSync('./certificates/localhost.fastplatform.eu/fullchain1.pem')
      },
      port: 8080,
      open: false
    },

    // animations: 'all', // --- includes all animations
    // https://quasar.dev/options/animations
    animations: [],

    // https://quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
      pwa: false
    },

    // https://quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
      workboxPluginMode: 'GenerateSW', // 'GenerateSW' or 'InjectManifest'
      workboxOptions: {}, // only for GenerateSW
      manifest: {
        name: 'FaST Farmer App',
        short_name: 'FaST Farmer App',
        description: 'FaST Farmer Application',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#1c9bcd',
        theme_color: '#1c9bcd',
        icons: [
          {
            src: 'icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png'
          },
          {
            src: 'icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src: 'icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png'
          },
          {
            src: 'icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
    cordova: {
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
      id: 'eu.fastplatform.mobile.farmer'
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-capacitor-apps/configuring-capacitor
    capacitor: {
      hideSplashscreen: false, // disable auto-hide SpashScreen
      iosStatusBarPadding: true, // add the dynamic top padding on iOS mobile devices
      // not the same app id for android and iOS
      // android: 'eu.fastplatform.mobile.farmer' & iOS: 'eu.fastplatform.mobile.farmer-app'
      id: ctx.target.android ? 'eu.fastplatform.mobile.farmer' : 'eu.fastplatform.mobile.farmer-app'
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
    electron: {
      bundler: 'packager', // 'packager' or 'builder'

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        appId: 'quasar-farmer-app'
      },

      // More info: https://quasar.dev/quasar-cli/developing-electron-apps/node-integration
      nodeIntegration: true,

      extendWebpack (cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      }
    },

    // Vendor libs configuration
    vendor: {
      // remove dynamically imported libs from vendor chunk
      remove: ['apexcharts', 'vue-apexcharts', 'canvg']
    }
  }
}
